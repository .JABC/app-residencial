import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { IMenuItemModel } from '@shared/model/menu-item.model';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html'
})
export class PanelComponent implements OnInit {

  @Input() panelTitle: string;
  @Input() items: IMenuItemModel;
  @Output() changes = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();

  constructor(private router: Router) { }

  ngOnInit() {

  }

  passActiveParent(elem: HTMLElement, path: string) {
    const classContained = elem.classList.value;
    const reg = new RegExp(/caption/);

    if (!reg.test(classContained)) {
      this.changes.emit(this.items);
      this.router.navigate([path]);
      this.hide();
    }
  }

  hide() {
    document.querySelector('.panel').classList.add('hide');
    this.close.emit();
  }

}
