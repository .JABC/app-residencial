

export interface IEntityModel {
    Id?: number;
    CreateDate?: Date;
    UpdateDate?: Date;
}

