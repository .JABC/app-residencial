import { TipoUsuario } from 'app/administracion/usuario/usuario.model';


export interface IMenuItemModel {
    path?: string;
    title: string;
    class: string;
    icon?: string;
    subItem?: ISubItemModel[];
    active?: boolean;
    roles?: TipoUsuario[];
}

export interface ISubItemModel extends IMenuItemModel {
    classChild?: string;
    // notification?: number;
}

// export interface ICardItemModel extends ItemModel {
//     countTotal?: number;
// }

