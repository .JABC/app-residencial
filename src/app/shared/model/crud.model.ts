import { Observable } from 'rxjs';

export interface ICRUD<T> {
    add(model: T): Observable<any>;
    select(filter?: T): Observable<T[]>;
    selectId(id: number): Observable<T>;
    update(model: T): Observable<any>;
    delete(model: T): Observable<any>;
}

export interface IManagerMethods<T> {
    add(model?: T): void;
    load(filter?: any): void;
    update(model?: T): void;
    delete(model?: T): void;
}

export class ActionEvent<T> {
    constructor(public action: T, public args?: any | any[]) {}
}



