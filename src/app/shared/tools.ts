import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Injector, TemplateRef } from '@angular/core';
import { appInjector } from './utils/injector';
import { ErrorService } from './utils/Errors/ErrorService';
import { Subscription } from 'rxjs';
import { ModalService } from './modal/modal.service';
import { IModalOption } from './modal/modal.model';


export const KEY_EVENT_CODE = {
    ESC: 27,
    ENTER: 13,
    SPACE: 32,
    TAB: 9,
    ARROWUP: 38,
    ARROWDOWN: 40
};

export const KEY_EVENT = {
    ESC: 'Escape',
    ENTER: 'Enter',
    SPACE: 'Space',
    TAB: 'Tab',
    ARROWUP: 'ArrowUp',
    ARROWDOWN: 'ArrowDown'
};

export abstract class AppTitle {

    private title = appInjector().get(Title);

    constructor(public route: ActivatedRoute) {
        setTimeout(() => this.setTitle(), 10);
    }

    private setTitle() {
        const subs = this.route.data.subscribe(
            (data: {title: string}) => this.title.setTitle(data.title),
            error => { throw error; },
            () => (subs as Subscription).unsubscribe()
        );
    }
}

// export function generateID(data: any[]): number {
//     let candidate = ;
//     while (data.findIndex(x => x.Id === candidate) > 0) {
//         candidate++;
//     }
//     return candidate;
// }

export enum Month {
    Jan = '01',
    Feb = '02',
    Mar = '03',
    Apr = '04',
    May = '05',
    Jun = '06',
    Jul = '07',
    Ago = '08',
    Sep = '09',
    Oct = '10',
    Nov = '11' ,
    Dec = '12'
}

export class DateTime {

    static now = `${DateTime.getDate()} ${DateTime.getTime()}`;

    static getDate(): string {
        let date = this.getDateTime('date'); // Jan 01 2020
        for (const prop in Month) {
            if (isNaN(Number(prop))) {
                date = date.replace(prop, Month[prop]).replace(' ', '-'); // mm-dd-yyyy
            }
        }
        // Change format from 'mm-dd-yyyy' to 'yyyy-mm-dd'
        date = `${date.split('-')[2]}-${date.split('-')[0]}-${date.split('-')[1]}`;
        return date;
    }

    static getTime(): string {
        return this.getDateTime('time');
    }

    static getDateTime(target: 'date' | 'time'): string {
        let currentdate = new Date().toString();
        currentdate = currentdate.slice(4, currentdate.indexOf('GMT') - 1); // 'Jun 08 2020 19:00:00'
        const time = currentdate.slice(currentdate.indexOf(':') - 3).trim(); // '19:00:00'
        const date = currentdate.slice(0, currentdate.indexOf(':') - 3); // 'Jun 08 2020'

        if (target === 'date') {
            return date;
        } else if (target === 'time') {
            return time;
        } else {
            throw new Error('Type not expecified...');
        }
    }
}

export function parseJwt(token: string) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map((x) => {
        return '%' + ('00' + x.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

export class RoleGuard {

    constructor(public role: string) { }

    validateRole(role: string): boolean {
        return this.role.toLocaleLowerCase() === role.toLocaleLowerCase();
    }

}

// export abstract class ModalWindow {

//     constructor(
//         private modal: ModalService
//     ) { }

//     show(content: TemplateRef<any>, options: IModalOption) {
//         this.modal.showModal(content, options);
//     }

//     close() {
//         this.modal.closeModal();
//     }

// }

export function equals(object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
        return false;
    }

    for (const key in object1) {
        if (object1[key] !== object2[key]) {
            return false;
        }
    }

    return true;
}

/**
 * Agrega objectos con valores no repetido a un array
 */
export class UniqueArray {

    private constructor() { }

    static add<T>(array: T[], item: T, insert: 'begin' | 'end' = 'end') {

        let exists: boolean;

        if (array.length === 0) {
            insert === 'begin' ? array.unshift(item) : array.push(item);
            return;
        }

        // Busca algun elemento que coincida si es asi frena el bucle
        // y coloca el campo exists como true, false si no encuentra alguno
        for (const value of array) {
            if (equals(value, item)) {
                exists = true;
                break;
            }

            exists = false;
        }

        if (!exists) {
            insert === 'begin' ? array.unshift(item) : array.push(item);
        }

    }

    static delete<T>(array: T[], item: T) {

        if (array.length === 0) {
            return;
        }

        array.forEach((value, index) => {
            const findedValue = equals(value, item);

            if (findedValue) {
                array.splice(index, 1);
            }
        });

    }

}
