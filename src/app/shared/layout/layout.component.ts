import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit, AfterViewInit {

  sidebarHide: boolean;

  @HostListener('window:resize')
  onWindowResize() {
    this.sidebarHide = true;
  }

  // @HostListener('window:load')
  // onWindowLoad() {
  //   this.router.navigate(['/home']);
  // }

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private title: Title) {

    }

  ngOnInit() {
    this.sidebarHide = true;
  }

  changeSidebarStatus() {
    this.sidebarHide = !this.sidebarHide;
  }

  ngAfterViewInit() {
    this.onInit();
  }

  private onInit() {
    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        setTimeout(() => this.spinner.show('BASE_SPINNER'));
        document.body.style.cursor = 'wait';
      } else if (
        e instanceof NavigationEnd ||
        e instanceof NavigationCancel) {
        setTimeout(() => {
          this.spinner.hide('BASE_SPINNER');
          document.body.style.cursor = 'default';
        });
      } else if (e instanceof NavigationError) {
        this.spinner.hide('BASE_SPINNER');
        document.body.style.cursor = 'default';
        throw e;
      }
    });
  }

}
