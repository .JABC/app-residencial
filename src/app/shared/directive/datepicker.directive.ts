import {
    Directive,
    OnInit,
    OnDestroy,
    ElementRef,
} from '@angular/core';
import { NgModel } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
    // tslint:disable-next-line
    selector: 'input[ngModel][ngbDatepicker]'
})
export class DatepickerDirective implements OnDestroy {

    // tslint:disable-next-line
    private _subscription: Subscription;

    constructor(public model: NgModel, public elem: ElementRef<HTMLInputElement>) {
        this._subscription = this.model.valueChanges.subscribe((value) => this.onInputChange(value));
    }

    onInputChange(value: any) {
        const regexp = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}/);
        if (value?.year) {
            setTimeout(
                () => this.model.viewToModelUpdate(`${value?.day}-${value?.month}-${value?.year}`)
            );
        } else if (regexp.test(value)) {
            this.DateFormat(value);
        }
    }

    private DateFormat(value: any) {
        this.model.reset();
        const date = (value as string).split('T')[0].split('-');
        this.model.valueAccessor.writeValue(`${date[2]}-${date[1]}-${date[0]}`);
        this.elem.nativeElement.value = `${date[2]}-${date[1]}-${date[0]}`;
    }

    ngOnDestroy(): void {
        this._subscription.unsubscribe();
    }

}
