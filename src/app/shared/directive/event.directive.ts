import { Directive, OnInit, HostListener, Output, EventEmitter, AfterViewInit } from '@angular/core';

// tslint:disable
@Directive({
    selector: '[ngOnInit]',
})
export class EventDirective implements OnInit {

    @Output('ngOnInit') _ngOnInit = new EventEmitter();

    constructor() { }

    ngOnInit() {
        this._ngOnInit.emit();
    }

}
