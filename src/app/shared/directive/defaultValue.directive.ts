import { Directive, Input, DoCheck } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
    // tslint:disable-next-line
    selector: '[default][ngModel]'
})
export class DefaultValueDirective implements DoCheck {

    // tslint:disable-next-line
    @Input('default') defaultValue: any;

    constructor(public model: NgModel) { }

    ngDoCheck() {
        this.setDefaultValue();
    }

    private setDefaultValue() {
        if (this.model.model === undefined) {
            setTimeout(() => this.model.valueAccessor.writeValue(this.defaultValue));
            setTimeout(() => this.model.viewToModelUpdate(this.defaultValue));
        }
    }

}
