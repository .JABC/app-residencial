import { Injectable, Injector } from '@angular/core';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { appInjector } from '../utils/injector';
import { IModalOption, TypeModal, ModalButton } from './modal.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalConfig: IModalOption = {};

  private _injector: Injector = appInjector();
  private _modalRef: NgbModalRef[] = [];
  private _modal: NgbModal;

  constructor() {
    this._modal = this._injector.get(NgbModal);
  }

  private setupModal(options?: IModalOption) {
    if (!options) {
      return;
    }

    switch (options.type) {
      case TypeModal.Information:
        this.modalConfig =  {
          title: options.title || 'Informacion',
          icon: options.icon || 'mdi mdi-information',
          backdrop: options.backdrop || 'static',
          color: options.color || '#0bb',
          size: options.size || 'sm',
          message: options.message,
          type: options.type
        };
        break;

      case TypeModal.Delete:
        this.modalConfig = {
          title: options.title || '¿Desea Eliminar Registro?',
          icon: options.icon || 'mdi mdi-delete',
          backdrop: options.backdrop || 'static',
          color: options.color || '#f62d51',
          size: options.size || 'sm',
          message: options.message || 'No se podra recuperar el registro',
          buttons: options.buttons || this.setModalButton(ModalButton.YesOrNo),
          type: options.type
        };
        break;

      case TypeModal.Question:
        this.modalConfig =  {
          title: options.title,
          icon: options.icon || 'mdi mdi-information',
          backdrop: options.backdrop || 'static',
          color: options.color || '#0bb',
          size: options.size || 'sm',
          message: options.message,
          buttons: options.buttons || this.setModalButton(ModalButton.YesOrNo),
          type: options.type
        };
        break;

      case TypeModal.Leave:
        this.modalConfig =  {
          title: options.title || '¿Desea Salir y Descartar los Cambios?',
          icon: options.icon || '',
          backdrop: options.backdrop || 'static',
          size: options.size || 'sm',
          message: options.message || 'Se podria perder informacion',
          buttons: options.buttons || this.setModalButton(ModalButton.YesOrNo),
          type: options.type
        };
        break;

      case TypeModal.Data:
        this.modalConfig = {
          keyboard: false,
          backdrop: options.backdrop || 'static',
          size: options.size || 'xl',
          type: options.type
        };
        break;

      case TypeModal.Custom:
        Object.assign(this.modalConfig, options);
        break;

      default:
      throw new Error(`Error: the current type '${TypeModal[options.type]}' doesn't exists`);
    }
  }

  private setModalButton(type: ModalButton): any {
    switch (type) {
        case ModalButton.YesOrNo:
          return [
              {
                label: 'Si',
                color: 'btn btn-info',
                action: 'close'
              },
              {
                label: 'No',
                color: 'btn',
                action: 'dismiss'
              }];

        case ModalButton.Confirm:
          return [
              {
                label: 'Aceptar',
                color: 'btn btn-info',
                action: 'close'
              }];

        default:
            throw new Error(`Error: the current type '${ModalButton[type]}' doesn't exists`);
    }
  }

  showModal(content: any, options?: IModalOption) {
    this.setupModal(options);
    const ref = this._modal.open(content, this.modalConfig);
    this._modalRef.push(ref);
    return ref.result;
  }

  closeModal() {
    this._modalRef.pop().close();
  }

  dismissModal() {
    this._modalRef.pop().dismiss();
  }

  dismissAll() {
    this._modal.dismissAll();
  }

}
