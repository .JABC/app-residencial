import { Component, OnInit } from '@angular/core';
import { ModalService } from './modal.service';
import { appInjector } from '../utils/injector';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  // tslint:disable-next-line
  host: {
    '[class.d-flex]': 'true',
    '[class.flex-column]': 'true',
    '[class.h-100]': 'true'
  }
})
export class ModalComponent implements OnInit {

  config: any;
  modal: ModalService;

  constructor() { }

  ngOnInit() {
    this.modal = appInjector().get(ModalService);
    this.config = this.modal.modalConfig;
  }

  onPressBtn(action: string) {
    if (action === 'close') {
      this.modal.closeModal();
    } else {
      this.modal.dismissModal();
    }
  }

}
