import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';


export interface IModalOption extends NgbModalOptions {
    title?: string;
    icon?: string;
    color?: string;
    message?: string;
    buttons?: any[];
    type?: TypeModal;
}

export enum TypeModal {
    Information,
    Delete,
    Question,
    Leave,
    Custom,
    Data
}

export enum ModalButton {
    YesOrNo,
    Confirm
}



