// @angular Module
import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Module
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JWTAuthInterceptor, AuthErrorInterceptor } from '../auth/http-interceptor';

// Ng-bootstrap
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

// Ngx-pagination
import { NgxPaginationModule } from 'ngx-pagination';

// Ngx-charts
import { NgxChartsModule } from '@swimlane/ngx-charts';

// Ngx-spinner
import { NgxSpinnerModule } from 'ngx-spinner';

// Component
import { PanelComponent } from './panel/panel.component';
import { LayoutComponent } from './layout/layout.component';
import { TopbarComponent } from './topbar/topbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { environment } from '@env/environment';

// Directive
import { ModalComponent } from './modal/modal.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NgbDateFormat } from './utils/dateFormat';
import { ToastComponent } from './toast/toast.component';
import { ToastService } from './toast/toast.service';
import { CoreModule } from '../core/core.module';
import { ErrorService } from './utils/Errors/ErrorService';
import { API_URL_TOKEN } from '../core/service/service.manager';
import { DatepickerDirective } from './directive/datepicker.directive';
import { NgApexchartsModule } from 'ng-apexcharts';
import { PerfilComponent } from './topbar/perfil/perfil.component';
import { PhonePipe } from './pipe/phone.pipe';
import { CedulaPipe } from './pipe/cedula.pipe';
import { ChangePasswordComponent } from './topbar/change-password/change-password.component';
import { DefaultValueDirective } from './directive/defaultValue.directive';

@NgModule({
  declarations: [
    LayoutComponent,
    TopbarComponent,
    SidebarComponent,
    PanelComponent,
    ModalComponent,
    NotFoundComponent,
    ToastComponent,
    DatepickerDirective,
    PerfilComponent,
    PhonePipe,
    CedulaPipe,
    ChangePasswordComponent,
    DefaultValueDirective
  ],
  imports: [
    CommonModule,
    NgxChartsModule,
    NgbModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    NgApexchartsModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    RouterModule
  ],
  exports: [
    NgbModule,
    ToastComponent,
    ModalComponent,
    PhonePipe,
    CedulaPipe,
    NgxChartsModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    DatepickerDirective,
    NgApexchartsModule,
    DefaultValueDirective
  ],
  providers: [
    { provide: API_URL_TOKEN, useValue: environment.api },
    { provide: NgbDateParserFormatter, useClass: NgbDateFormat },
    { provide: ErrorService, useClass: ErrorService },
    { provide: ErrorHandler, useClass: ErrorService },
    { provide: HTTP_INTERCEPTORS, useClass: JWTAuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AuthErrorInterceptor, multi: true },
    ToastService
  ]
})
export class SharedModule { }
