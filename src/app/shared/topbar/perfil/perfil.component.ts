﻿import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { parseJwt } from '@shared/tools';
import { AuthService } from 'app/auth/auth.service';
import { ModalComponent } from '@shared/modal/modal.component';
import { EMPTY } from 'rxjs';
import { TipoUsuario } from 'app/administracion/usuario/usuario.model';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html'
})
export class PerfilComponent implements OnInit {

    @Input() service: AuthService;

    @ViewChild('component') component: any;
    model: any = {};
    imgLoaded: boolean;
    get canDeleteResidencia(): boolean {
        return localStorage.getItem('residencia') !== null;
    }

    role = TipoUsuario;

    constructor(public modal: ModalService) { }

    ngOnInit(): void {
        this.imgLoaded = false;
        try {
            Object.assign(this.model,
                parseJwt(this.service.token.getToken()?.token));
        } catch { return; }
    }

    open() {
        this.modal.showModal(this.component, { type: TypeModal.Data });
    }

    deleteResidencia() {
        this.modal.showModal(ModalComponent, {
            type: TypeModal.Delete,
            title: '¿Borrar Residencia Registrada?',
            message: 'Ingresar codigo de residencia para restaurar'
        })
        .then(() => {
            if (localStorage.getItem('residencia')) {
                localStorage.removeItem('residencia');
            }
        })
        .catch(() => EMPTY);
    }

    close() {
        this.modal.closeModal();
    }

}
