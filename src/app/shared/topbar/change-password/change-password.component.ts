import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { AuthService } from 'app/auth/auth.service';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { ActionEvent } from '@shared/model/crud.model';
import { FormAction } from '@core/template/form/form.component';
import { NgForm } from '@angular/forms';
import { parseJwt } from '@shared/tools';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    providers: [AuthService]
})
export class ChangePasswordComponent implements OnInit {

    @ViewChild('component') component: TemplateRef<any>;
    @ViewChild('form') form: NgForm;

    constructor(public service: AuthService, public modal: ModalService) { }

    ngOnInit(): void { }

    open() {
        this.modal.showModal(this.component, { type: TypeModal.Data });
    }

    close() {
        this.modal.closeModal();
    }

    changes(event: ActionEvent<FormAction>) {
        switch (event.action) {
            case FormAction.Submit:
		this.service.model.UserName = parseJwt(this.service.token.getToken()?.token).unique_name;
                this.service.changePassword();
                this.service.model = {};
                break;

            case FormAction.Cancel:
                this.close();
                this.service.model = {};
                break;

            default:
                break;
        }
    }


}
