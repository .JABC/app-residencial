import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router';
import { AuthService } from 'app/auth/auth.service';
import { parseJwt } from '@shared/tools';
import { ModalService } from '@shared/modal/modal.service';
import { TipoUsuario } from 'app/administracion/usuario/usuario.model';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  providers: [AuthService]
})
export class TopbarComponent implements OnInit {

  userInfo: any = {};
  isDropdown = false;
  breadcrumb = [];
  role = TipoUsuario;

  constructor(private router: Router, public service: AuthService) {
    this.router.events.subscribe(
      (event: any) => {
        if (event instanceof NavigationEnd) {
          this.createBreadcrum();
        }
      }
    );
  }

  ngOnInit() {
    try {
      Object.assign(this.userInfo,
        parseJwt(this.service.token.getToken()?.token));
    } catch {
      return;
    }
  }

  private createBreadcrum() {
    const routes = this.router.url.split('/').slice(1);
    this.breadcrumb = [];
    routes.forEach((item, index, array) => {
      this.breadcrumb.push({
        title: item,
        route: index > 0 ? `/${array[index - 1]}/${array[index]}` : `/${array[index]}`
      });
    });
  }

}
