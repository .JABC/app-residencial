import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'cedula'})
export class CedulaPipe implements PipeTransform {
    transform(value: any): any {
        const s2 = ('' + value).replace(/\D/g, '');
        const m = s2.match(/^(\d{3})(\d{7})(\d{1})$/);

        return !m ? null : m[1] + '-' + m[2] + '-' + m[3];
    }
}
