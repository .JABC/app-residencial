import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'dynamic'})
export class DynamicPipe implements PipeTransform {
    transform(value: any, type: unknown, args: any[]): any {
        if (!type) {
            return value;
        }

        const pipe = typeof type;
        alert(pipe);
    }
}