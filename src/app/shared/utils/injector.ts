import { Injector } from '@angular/core';

let rootInjector: Injector;

export const appInjector = (injector?: Injector): Injector => {
  if (injector) {
    rootInjector = injector;
  }
  return rootInjector;
};
