import { Injectable, ErrorHandler, NgZone, Injector } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService, TypeToast, Toast } from '../../toast/toast.service';

// export function errorHandler(debug) {
//     try {
//         return new ErrorService(debug);
//     } catch (e) {
//         throw e;
//     }
// }

@Injectable()
export class ErrorService implements ErrorHandler {

    private ngZone: NgZone;
    private toast: ToastService;
    private readonly debug = true;
    private readonly typeToast = TypeToast.Error;

    logs: any[] = [];

    constructor(private injector: Injector) {
        this.ngZone = this.injector.get(NgZone);
        this.toast = this.injector.get(ToastService);
    }

    handleError(error: any) {
        if (error instanceof Error) {
            this.reportError(error);
        } else if (error instanceof HttpErrorResponse) {
            this.reportHttpError(error);
        } else {
            console.error(error.toString());
        }
    }

    private reportError(error: Error) { // Only While development 
        this.run({
            message: 'Ha ocurrido un error',
            type: this.typeToast
        });
        console.error(error);
    }

    private reportHttpError(error: HttpErrorResponse) {
        // let httpTypeToast = this.typeToast;
        // if (this.debug) {
        if (error.error?.isTrusted) {
            Object.assign(error, {
                status: '',
                error: 'Servidor no disponible',
                url: ''
            });
        }
            // else if (!navigator.onLine) {
            //     Object.assign(error, {
            //         status: '',
            //         statusText: 'Conexion a Internet',
            //         message: 'Revise su conexion a internet',
            //         url: ''
            //     });
            //     httpTypeToast = TypeToast.Warning;
            // }
        this.run({
            message: `${error.error}`,
            type: this.typeToast
        });
        // this.run({
        //     title: `Error de Servidor`,
        //     body: `Ha ocurrido un error en el servidor`,
        //     type: this.typeToast
        // });
        // }
        console.error(error);
    }

    private run(toast: Toast) {
        this.ngZone.run(
            () => this.toast.show(toast)
        );
    }
}


