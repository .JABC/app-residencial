import { Injectable, ɵbypassSanitizationTrustResourceUrl } from '@angular/core';

export interface Toast {
    message: string;
    class?: string;
    delay?: number;
    autohide?: boolean;
    icon?: string;
    type: TypeToast;
}

export enum TypeToast {
    Success,
    Warning,
    Error,
    Notification
}

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    toasts: Toast[] = [];

    constructor() { }

    show(toast: Toast) {
        this.toasts.push(this.setupToast(toast));
    }

    remove(toast: Toast) {
        this.toasts = this.toasts.filter(t => t !== toast);
    }

    private setupToast(toast: Toast) {
        const config: Toast = {
            message: toast.message,
            autohide: true,
            delay: toast?.delay ? toast.delay : 5000,
            type: toast.type
        };
        switch (toast.type) {
        case TypeToast.Success:
            Object.assign(config, {
                class: 'toast-success toast-sm',
                icon: 'mdi mdi-check mdi-bold'
            });
            break;

        case TypeToast.Warning:
            Object.assign(config, {
                class: 'toast-warning toast-sm',
                icon: 'mdi mdi-exclamation mdi-bold'
            });
            break;

        case TypeToast.Error:
            Object.assign(config, {
                class: 'toast-danger toast-sm',
                icon: 'mdi mdi-close mdi-bold',
            });
            break;

        case TypeToast.Notification:
            Object.assign(config, {
                class: 'toast-info toast-sm',
                icon: 'mdi mdi-alert-octagram'
            });
            break;

        default:
            this.setupToast(
            {
                message: `the current type '${toast.type}' doesn't exists`,
                type: TypeToast.Error
            });
            break;
        }
        return config;
    }
}

