import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastService } from './toast.service';

@Component({
  selector: 'app-toast',
  template: `
    <ngb-toast
      *ngFor="let toast of toastService.toasts"
      [class]="toast.class"
      [autohide]="toast?.autohide"
      [delay]="toast?.delay"
      (hide)="toastService.remove(toast)"
      (click)="toastService.remove(toast)">

      <ng-template [ngIf]="isTemplate(toast)" [ngIfElse]="text">
        <ng-template [ngTemplateOutlet]="'toast.title'"></ng-template>
      </ng-template>

      <ng-template #text>
        <div class="toast-icon">
          <i [ngClass]="[toast.icon]"></i>
        </div>
        <div class="toast-text">
          <small class="toast-message">{{ toast.message }}</small>
        </div>
      </ng-template>



    </ngb-toast>
  `,
  styles: [`
    .toast {
      position: fixed;
      top: 8px;
      left: 0;
      right: 0;
      margin: auto;
      z-index: 99999;
    }
  `]
})
export class ToastComponent implements OnInit {

  constructor(public toastService: ToastService) { }

  ngOnInit() { }

  isTemplate(toast) { return toast.textOrTpl instanceof TemplateRef; }

}
