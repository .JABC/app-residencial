import { IMenuItemModel, ISubItemModel } from '../model/menu-item.model';
import { TipoUsuario } from 'app/administracion/usuario/usuario.model';

const ADMINISTRACION_SUBITEM: ISubItemModel[] = [
    {
        title: 'Administrar Registros',
        icon: '',
        class: 'caption-header',
        classChild: 'caption'
    },
    {
        path: '/administracion/residente',
        title: 'Residente',
        icon: 'mdi mdi-account-box-multiple',
        class: '',
        classChild: ''
    },
    {
        path: '/administracion/residencia',
        title: 'Residencia',
        icon: 'mdi mdi-home-city',
        class: '',
        classChild: ''
    },
    {
        path: '/administracion/calle',
        title: 'Calle',
        icon: 'mdi mdi-road-variant',
        class: '',
        classChild: ''
    },
    {
        path: '/administracion/vehiculo',
        title: 'Vehiculo',
        icon: 'mdi mdi-car-multiple',
        class: '',
        classChild: ''
    },
    {
        path: '/administracion/visita',
        title: 'Visitas',
        icon: 'mdi mdi-home-import-outline',
        class: '',
        classChild: ''
    },
    {
        path: '/administracion/area-comun',
        title: 'Areas Comunes',
        icon: 'mdi mdi-grid',
        class: '',
        classChild: ''
    }
];

const ACCESO_SUBITEM: ISubItemModel[] = [
    {
        title: 'Acceso de Persona',
        icon: '',
        class: 'caption-header',
        classChild: 'caption'
    },
    {
        path: '/acceso/flujo',
        title: 'Flujo de Visitas',
        icon: 'mdi mdi-login',
        class: '',
        classChild: ''
    },
];

// export const USUARIO_SUBITEM: ISubItemModel[] = [
//     {
//         path: '',
//         title: 'Mi Residencia',
//         icon: 'mdi mdi-home-circle',
//         class: '',
//         classChild: '',
//         notification: 0
//     },
//     {
//         path: '',
//         title: 'Actividad',
//         icon: 'mdi mdi-text-subject',
//         class: '',
//         classChild: '',
//         notification: 0
//     },
// ];

// export const ADMINISTRACION_SUBITEM: ISubItemModel[] = [
//     {
//         path: '/administracion/usuario',
//         title: 'Usuarios',
//         icon: 'mdi mdi-clipboard-account',
//         class: '',
//         classChild: ''
//     },
//     // {
//     //     path: '/administracion/tarea',
//     //     title: 'Tareas',
//     //     icon: 'mdi mdi-wrench',
//     //     class: '',
//     //     classChild: '',
//     //     notification: 0
//     // },
//     // {
//     //     path: '/administracion/reporte',
//     //     title: 'Reportes',
//     //     icon: 'mdi mdi-file-document-edit',
//     //     class: '',
//     //     classChild: ''
//     // },
// ];

// export const PAGOS_SUBITEM: ISubItemModel[] = [
//     {
//         title: 'Mis Pagos',
//         icon: '',
//         class: 'caption-header',
//         classChild: 'caption',
//         notification: 0
//     },
//     {
//         path: '',
//         title: 'Recibos',
//         icon: 'mdi mdi-logout',
//         class: '',
//         classChild: '',
//         notification: 0
//     },
// ];

export const COMUNICACION_SUBITEM: ISubItemModel[] = [
    {
        title: 'Comunicacion Administrador - Residente',
        icon: '',
        class: 'caption-header',
        classChild: 'caption'
    },
    {
        path: '/comunicacion/noticia',
        title: 'Noticias',
        icon: 'mdi mdi-login',
        class: '',
        classChild: ''
    },
    {
        path: '/comunicacion/comunicado',
        title: 'Comunicados',
        icon: 'mdi mdi-login',
        class: '',
        classChild: ''
    }
];

/********************** Sidebar Items ****************************/
export const MENU_ITEM: IMenuItemModel[] = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        icon: 'mdi mdi-chart-bar-stacked',
        class: '',
        active: false,
        roles: [TipoUsuario.Administrador]
    },
    {
        title: 'Administracion',
        icon: 'mdi mdi-file-multiple-outline',
        subItem: ADMINISTRACION_SUBITEM,
        class: '',
        active: false,
        roles: [TipoUsuario.Administrador]
    },
    {
        path: '/acceso/flujo',
        title: 'Acceso',
        icon: 'mdi mdi-account-cancel',
        class: '',
        active: false,
        roles: [TipoUsuario.Administrador]
    },
    // {
    //     title: 'Acceso',
    //     icon: 'mdi mdi-account-cancel',
    //     subItem: ACCESO_SUBITEM,
    //     class: '',
    //     active: false
    // },
    {
        path: '/residencia',
        title: 'Residencia',
        icon: 'mdi mdi-home-flood',
        class: '',
        active: false,
        roles: [TipoUsuario.Administrador, TipoUsuario.Residente]
    },
    {
        title: 'Comunicacion',
        icon: 'mdi mdi-archive-arrow-up',
        class: '',
        subItem: COMUNICACION_SUBITEM,
        active: false,
        roles: [TipoUsuario.Administrador]
    },
    // {
    //     title: 'Pagos',
    //     // subItem: PAGOS_SUBITEM,
    //     icon: 'mdi mdi-cash-multiple',
    //     class: '',
    //     active: false
    // },
    // {
    //     path: '',
    //     title: 'Eventos',
    //     icon: 'mdi mdi-notebook',
    //     class: '',
    //     active: false
    // },
    // {
    //     title: 'Usuario',
    //     subItem: USUARIO_SUBITEM,
    //     icon: 'mdi mdi-account-group',
    //     class: '',
    //     active: false
    // },
    // {
    //     path: '/administracion',
    //     title: 'Administracion',
    //     icon: 'mdi mdi-file-chart',
    //     subItem: ADMINISTRACION_SUBITEM,
    //     class: '',
    //     active: false,
    //     roles: [TipoUsuario.Administrador]
    // }
];


