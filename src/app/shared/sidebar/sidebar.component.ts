import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IMenuItemModel, ISubItemModel } from '../model/menu-item.model';
import { MENU_ITEM } from './item.data';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ModalService } from '@shared/modal/modal.service';
import { ModalComponent } from '@shared/modal/modal.component';
import { TypeModal } from '@shared/modal/modal.model';
import { EMPTY } from 'rxjs';
import { AuthService } from 'app/auth/auth.service';
import { parseJwt } from '@shared/tools';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  providers: [AuthService]
})
export class SidebarComponent implements OnInit {

  @Output() public toggleSidebar = new EventEmitter<any>();
  isHide: boolean;
  menuItems: IMenuItemModel[];
  subMenuItems: IMenuItemModel;
  panelTitle: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public service: AuthService) { }

  ngOnInit() {
    this.isHide = true;
    const user = parseJwt(this.service.token.getToken()?.token);
    this.menuItems = MENU_ITEM.filter(item => item.roles.indexOf(parseInt(user?.role, 10)) > -1);
    this.activeItem(); // Activa item del sidebar al iniciar
  }

  // Envia los subitems del padre al panel para asi mostrarlos
  // si no tiene subitems solo navega a su ruta
  configPanel(token: ISubItemModel, title: string) {
    const subItems =  MENU_ITEM.find(
      item => item.subItem === token.subItem)/*.subItem*/;

    if (subItems.subItem !== undefined) {
      this.subMenuItems = subItems;
      this.panelTitle = `Control de ${title}`;
      document.querySelector('.panel').classList.remove('hide');
    } else if (token.path !== undefined) {
      this.router.navigate([token.path])
        .then(() => this.activeItem())
        // .then(() => this.menuItems.forEach(
        //   item => item === token ? item.active = true : item.active = false))
        .catch(error => { throw error; });
      document.querySelector('.panel').classList.add('hide');
      this.hideSidebar();
    }

    if (window.innerWidth > 767) {
      this.hideSidebar();
    }
  }

  // // Desde panel se manda al item seleccionado para activar a su
  // // padre en el sidebar
  // activeParent(items: ISubItemModel[]) {
  //   this.menuItems.forEach(item => {
  //     if (item.subItem === items) {
  //       item.active = true;
  //     } else {
  //       item.active = false;
  //     }
  //   });
  // }

  // Espera a que la navegacion termine para activar el item padre
  onPanelAction() {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.activeItem();
      }
    });
  }

  // Al cargar la app se verifica si la url coincide con algun item
  // para poder activarlo
  activeItem() {
    const currentRouteUrl = this.router.url;
    const regExp = new RegExp(currentRouteUrl.split('/')[1], 'ig');
    this.menuItems.forEach(item => {
      if (regExp.test(item.title)) {
        item.active = true;
      } else {
        item.active = false;
      }
    });
  }

  hideSidebar() {
    this.toggleSidebar.emit();
  }

}
