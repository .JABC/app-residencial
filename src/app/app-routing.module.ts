import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './shared/layout/layout.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { AuthGuard, AuthRoleGuard } from './auth/auth.guard';
import { TipoUsuario } from './administracion/usuario/usuario.model';

const routes: Routes = [
  {
    path: 'account',
    loadChildren: () => import('./auth/auth.module')
                       .then(m => m.AuthModule)
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: '',
    component: LayoutComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module')
          .then(m => m.DashboardModule),
        // canActivate: [AuthRoleGuard],
        // data: { roles: [TipoUsuario.Administrador] }
      },
      // {
      //   path: 'registro',
      //   loadChildren: () => import('./registro/registro.module')
      //     .then(m => m.RegistroModule),
      //   // canActivate: [AuthRoleGuard],
      //   // data: { roles: [TipoUsuario.Administrador] }
      // },
      {
        path: 'acceso',
        loadChildren: () => import('./acceso/acceso.module')
          .then(m => m.AccesoModule),
        // canActivate: [AuthRoleGuard],
        // data: { roles: [TipoUsuario.Administrador, TipoUsuario.Guardia] }
      },
      {
        path: 'residencia',
        loadChildren: () => import('./mi-residencia/mi-residencia.module')
          .then(m => m.MiResidenciaModule),
        // canActivate: [AuthRoleGuard],
        // data: { roles: [TipoUsuario.Administrador, TipoUsuario.Residente] }
      },
      {
        path: 'administracion',
        loadChildren: () => import('./administracion/administracion.module')
          .then(m => m.AdminitracionModule),
        // canActivate: [AuthRoleGuard],
        // data: { roles: [TipoUsuario.Administrador] }
      },
      {
        path: 'comunicacion',
        loadChildren: () => import('./comunicacion/comunicacion.module')
          .then(m => m.ComunicacionModule),
        // canActivate: [AuthRoleGuard],
        // data: { roles: [TipoUsuario.Administrador] }
      }
    ]
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [AuthGuard, AuthRoleGuard]
})
export class AppRoutingModule { }
