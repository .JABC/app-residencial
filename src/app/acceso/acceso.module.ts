import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthRoleGuard } from 'app/auth/auth.guard';
import { TipoUsuario } from 'app/administracion/usuario/usuario.model';

const routes: Routes = [
  { path: '', redirectTo: 'flujo', pathMatch: 'full' },
    {
        path: '',
        children: [
            {
                path: 'flujo',
                loadChildren: () => import('./flujo-visita/flujo-visita.module')
                    .then(m => m.FlujoVisitaModule),
                // canActivate: [AuthRoleGuard],
                // data: { roles: [TipoUsuario.Administrador, TipoUsuario.Guardia] }
            },
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ],
    providers: [AuthRoleGuard]
})
export class AccesoModule { }
