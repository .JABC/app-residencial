import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SearchColumn } from '@core/template/search/search.component';
import { Subscription } from 'rxjs';
import { VisitaService } from '@adm/visita/visita.service';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateTime } from '@shared/tools';
import { Filter, EFilterOperator } from '@core/service/service.model';

@Component({
    selector: 'app-flujo-visita-form',
    templateUrl: './form.component.html'
})
export class FlujoVisitaFormComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild('form') form: NgForm;
    @ViewChild('focus') focusControl: ElementRef<HTMLInputElement>;

    canEdit = false;
    private action: 'add' | 'edit' = 'add';
    private onSearch = false;

    residenciaColumns: SearchColumn[] = [
        new SearchColumn('Codigo', 'UniqueIdentifier'),
        new SearchColumn('Numero')
    ];

    private subscription: Subscription;

    constructor(
        public service: VisitaService,
        public residenciaService: ResidenciaService,
        public spinner: NgxSpinnerService) { }

    ngOnInit(): void {
        this.service.subscribe();
        this.residenciaService.subscribe();
        this.subscription = this.service.onLoad.subscribe(
            (data: any[]) => this.assignModel(data)
        );
    }

    ngAfterViewInit() {
        this.focus();
    }

    focus() {
        this.focusControl.nativeElement.focus();
    }

    selectedItem(item: any) {
        this.service.model.ResidenciaId = item.Id;
        this.service.model.ResidenciaNumero = item.Numero;
    }

    add() {
        if (this.canEdit) {
            this.addVisita();
        } else {
            this.searchByDNI();
        }
    }

    addDateTime(arg: 'llegada' | 'salida') {
        const date = new Date().toISOString();

        if (arg === 'llegada') {
            this.service.model.Llegada = date;
        } else {
            this.service.model.Salida = date;
        }
    }

    private searchByDNI() {
        const newFilter = new Filter('VisitanteDNI', this.service.model.VisitanteDNI, EFilterOperator.Equals);
        this.service.config.Filters = [newFilter];
        this.service.load();
        this.onSearch = true;
    }

    private assignModel(data: any[]) {
        if (!this.onSearch) {
            return;
        }

        if (data.length > 0) {
            const item = data.reverse()[0]; // elige el ultimo visitante agregado
            if (!item?.Salida) {
                this.action = 'edit';
                this.residenciaService.model.Numero = item.ResidenciaNumero;
                Object.assign(this.service.model, item);
            } else {
                this.action = 'add';
                this.service.model.VisitanteNombre = item.VisitanteNombre;
                this.service.model.Reportado = item.Reportado;
                this.service.model.TieneEntrada = item.TieneEntrada;
            }
        }

        this.canEdit = true;
    }

    private addVisita() {
        if (this.action === 'edit') {
            this.service.postMethod('update', this.service.model);
            alert('Edit');
        } else {
            this.service.postMethod('add', this.service.model);
            alert('Add');
        }

        this.cancel();
        this.focus();
    }

    cancel() {
        this.service.model = {};
        this.residenciaService.model = {};
        this.form.reset();
        this.canEdit = false;
        this.onSearch = false;
        this.action = 'add';
    }

    ngOnDestroy() {
        this.service.unsubscribe();
        this.residenciaService.unsubscribe();
        this.subscription.unsubscribe();
        this.cancel();
    }
}
