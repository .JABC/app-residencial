import { Routes, RouterModule } from '@angular/router';
import { FlujoVisitaComponent } from './flujo-visita.component';
import { NgModule } from '@angular/core';
import { FlujoVisitaFormComponent } from './form/form.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { VisitaFormModule } from '@adm/visita/form/form.module';
import { ResidenciaFormModule } from '@adm/residencia/form/form.module';

const routes: Routes = [
    {
        path: '',
        component: FlujoVisitaComponent,
        data: { title: 'Flujo de Visitas' }
    }
];

@NgModule({
    declarations: [
        FlujoVisitaComponent,
        FlujoVisitaFormComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        VisitaFormModule,
        ResidenciaFormModule,
        RouterModule.forChild(routes)
    ],
    providers: []
})
export class FlujoVisitaModule { }
