import { Component, OnInit } from '@angular/core';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-flujo-visita',
    templateUrl: './flujo-visita.component.html',
    styles: [
        ` .entrada-salida .card-header {
            border-right: 45px solid transparent;
            border-bottom: 35px solid #0bb;
            box-sizing: border-box;
            height: 35px;
            line-height: 35px;
            background-color: #2e3c44;
            padding: 0 12px;
            color: #fff;
            font-weight: 600;
        }

        `]
})
export class FlujoVisitaComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
