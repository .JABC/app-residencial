import { Injectable, Inject } from '@angular/core';
import { TokenService } from './token.service';
import { HttpClient } from '@angular/common/http';
import { ErrorService } from '@shared/utils/Errors/ErrorService';
import { ToastService, TypeToast } from '@shared/toast/toast.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { API_URL_TOKEN } from '@core/service/service.manager';
import { NgForm } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class AuthService {

  model: any = {};
  private _loading = false;

  get loading(): boolean {
    return this._loading;
  }

  set loading(value) {
    this._loading = value;
    this._loading ?
      setTimeout(() => this.spinner.show('AUTH_SPINNER')) :
      setTimeout(() => this.spinner.hide('AUTH_SPINNER'));
  }

  constructor(
    public token: TokenService,
    public http: HttpClient,
    public errorService: ErrorService,
    public toast: ToastService,
    public spinner: NgxSpinnerService,
    public router: Router,
    @Inject(API_URL_TOKEN) private API_URL: string
  ) { }

  login(form: NgForm) {
    this.loading = true;
    const req = this.http.post(`${this.API_URL}/auth/login`, this.model).subscribe(
      (token) => {
        this.router.navigate(['/']);
        this.token.setToken(token['Value']);
        // alert(JSON.stringify(parseJwt(token['Value']?.token), null, '\r'));
        form.reset();
        this.loading = false;
        req.unsubscribe();
      },
      (error: any) => {
        this.loading = false;
        this.errorService.handleError(error);
      }
    );
  }

  register(form: NgForm) {
    // alert(JSON.stringify(this.model, null, '\t'));
    this.loading = true;
    const req = this.http.post(`${this.API_URL}/auth/create`, this.model).subscribe(
      () => {
        this.loading = false;
        form.reset();
        this.router.navigate(['/account/login']);
        this.toast.show({
          message: 'Se registro correctamente, Inicie sesion',
          type: TypeToast.Success
        });
        req.unsubscribe();
      },
      (error: any) => {
        this.loading = false;
        this.errorService.handleError(error);
      }
    );
  }

  changePassword() {
    this.loading = true;
    const req = this.http.post(`${this.API_URL}/auth/changepassword`, this.model).subscribe(
      () => {
        this.toast.show({
          message: 'Su contraseña ha sido cambiada correctamente',
          type: TypeToast.Success
        });
        this.loading = false;
        req.unsubscribe();
      },
      (error: any) => {
        this.loading = false;
        this.errorService.handleError(error);
      }
    );
  }

  logout() {
    this.token.removeToken();
    this.router.navigate(['/account/login']);
  }

}
