import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';
import { AuthService } from '@auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent extends AppTitle implements OnInit {

    constructor(public service: AuthService) {
        super();
    }

    ngOnInit() {
        if (!this.service.token.isOutsideDate()) {
            this.service.router.navigate(['/']);
        }
    }
}
