import { Injectable } from '@angular/core';
import { DateTime, parseJwt } from '@shared/tools';

@Injectable({ providedIn: 'root' })
export class TokenService {

    readonly tokenName = 'token';
    readonly headerPrefix = 'Bearer';
    readonly headerType = 'Authorization';

    constructor() { }

    getToken() {
        const token = localStorage.getItem(this.tokenName);

        return JSON.parse(token);
    }

    setToken(tokenObject: any) {
        const token = JSON.stringify(tokenObject);
        localStorage.setItem(this.tokenName, token);
    }

    isOutsideDate(): boolean {
        const token = this.getToken()
            ? parseJwt(this.getToken()?.token)
            : null;

        // if (Date.now() >= (token?.exp * 1000) || token == null) {
        //     return true;
        // }

        return Date.now() >= (token?.exp * 1000) || token == null;
    }

    removeToken(tokenName?: string) {
        if (tokenName) {
            localStorage.removeItem(this.tokenName);
            return;
        }

        localStorage.clear();
    }

}
