import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@auth/auth.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html'
})
export class RegisterComponent extends AppTitle implements OnInit {

    constructor(public service: AuthService) {
        super();
    }

    ngOnInit(): void { }
}
