import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class JWTAuthInterceptor implements HttpInterceptor {

  // tslint:disable-next-line
  constructor(private _token: TokenService, public router: Router) {}

  intercept(req: HttpRequest<any> , next: HttpHandler): Observable <HttpEvent<any>> {
    const token = this._token.getToken();

    if (token && !this._token.isOutsideDate()) {
      req = req.clone({
        headers: req.headers.set(
          this._token.headerType,
          `${this._token.headerPrefix} ${token.token}`
        )
      });
    }

    return next.handle(req);
  }
}

@Injectable({ providedIn: 'root' })
export class AuthErrorInterceptor implements HttpInterceptor {

  constructor(public auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(catchError(err => {

      if (err instanceof HttpErrorResponse) {
        if ([401, 403].indexOf(err.status) !== -1) {
          this.auth.logout();
          location.reload();
        }
      }

      return throwError(err);
    }));
  }

}
