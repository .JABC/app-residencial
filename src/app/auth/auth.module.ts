import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { TokenService } from './token.service';
import { AuthService } from './auth.service';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {
        path: '',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                component: LoginComponent,
                data: { title: 'Iniciar Sesion' }
            },
            {
                path: 'register',
                component: RegisterComponent,
                data: { title: 'Registrarme' }
            }
        ]
    }
];

@NgModule({
    declarations: [
        AuthComponent,
        LoginComponent,
        RegisterComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [
        TokenService,
        AuthService

    ],
})
export class AuthModule { }
