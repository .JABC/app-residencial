import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { TokenService } from './token.service';
import { parseJwt } from '@shared/tools';
import { TipoUsuario } from '@adm/usuario/usuario.model';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    public token: TokenService,
    public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    try {
      if (this.token.isOutsideDate()) {
        this.redirectToLogin(state);
        return false;
      }
    } catch {
      this.redirectToLogin(state);
    }

    return true;
  }

  private redirectToLogin(state: RouterStateSnapshot) {
    this.token.removeToken();
    this.router.navigate(['account/login'], { queryParams: { returnUrl: state.url } });
  }

}

@Injectable({ providedIn: 'root' })
export class AuthRoleGuard implements CanActivate, CanActivateChild {

  constructor(
    public router: Router,
    public token: TokenService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const currentUser = parseJwt(this.token.getToken().token);

    if (currentUser) {
      const roles = route.data?.roles;

      if (route.url[0].path === 'dashboard') {
        switch (parseInt(currentUser.role, 10)) {
          case TipoUsuario.Guardia:
            this.router.navigate(['/acceso/entrada-y-salida']);
            return false;
          case TipoUsuario.Residente:
            this.router.navigate(['/residencia']);
            return false;
          default:
            return true;
        }
      }

      if (roles && roles.indexOf(parseInt(currentUser.role, 10)) === -1) {
        this.router.navigate(['/']);
        return false;
      }

      return true;
    }

    this.router.navigate(['account/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }

}
