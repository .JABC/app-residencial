import { Component, OnInit, OnDestroy } from '@angular/core';
import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit, OnDestroy {

  constructor(public service: DashboardService) {
  }

  ngOnInit() {
    this.service.subscribe();
  }

  ngOnDestroy() {
    this.service.unsubscribe();
  }

}
