// tslint:disable
export const dataColumnChart = {
  // series: [
  //   {
  //     name: "Residencias",
  //     data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
  //   },
  //   {
  //     name: "Residentes",
  //     data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
  //   },
  //   {
  //     name: "Vehiculos",
  //     data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
  //   }
  // ],
  chart: {
    type: "bar",
    height: "400"
  },
  plotOptions: {
    bar: {
      horizontal: false,
      columnWidth: "55%"
    }
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    show: true,
    width: 2,
    colors: ["transparent"]
  },
  xaxis: {
    categories: [
      "Ene",
      "Feb",
      "Mar",
      "Abr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ]
  },
  fill: {
    opacity: 1
  }
};


export const dataPieChart = {
    labels: ['Activos', 'Inactivos'],
    chart: {
        height: '200',
        type: 'donut'
    },
    colors: ['#00d1b2', '#2a3b47']
  }

export const dataRadialChart = {
    chart: {
        type: "radialBar"
    },
    labels: ["Apartamento", "Negocio", "Desconocido", "Casa"].reverse()
};


