import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DashboardService } from '../dashboard.service';
import { dataColumnChart, dataPieChart, dataRadialChart } from '../data';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSpinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { IVisitaModel } from '@adm/visita/visita.model';

@Component({
  selector: 'app-dashboard-residencial',
  templateUrl: './dashboard-residencial.component.html',
  styles: [`
        .text-vertical {
          width: 30px;
          writing-mode: vertical-lr;
          line-height: 8px;
          text-align: center;
        }
  `]
})
export class DashboardResidencialComponent extends AppTitle
  implements OnInit, OnDestroy {

  dataColumnChart: any;
  dataPieChart: any;
  dataRadialChart: any;
  loaded = false;
  model = 1;
  private subscription = new Subscription();

  constructor(
    public service: DashboardService,
    public spinner: NgxSpinnerService,
    public route: ActivatedRoute) {
    super(route);
  }

  ngOnInit() {
    this.dataColumnChart = dataColumnChart;
    this.dataPieChart = dataPieChart;
    this.dataRadialChart = dataRadialChart;
    //
    this.subscription.add(this.service.loading.subscribe(
      (loading: boolean) => this.showSpinner(loading)
    ));
    this.service.load();
    this.subscription.add(this.service.onLoad.subscribe(
      () => this.assignModel()
    ));
  }

  private assignModel() {
    this.dataPieChart = { ...{ series: this.activosInactivosResidente }, ...dataPieChart};
    //
    this.dataRadialChart = { ...{
      series: this.tipoResidenciaLength,
      plotOptions: {
        radialBar: {
          dataLabels: {
            total: {
              show: true,
              label: 'Total',
              formatter: () => this.tipoResidenciaLength.reduce((prev, next) => prev + next)
            }
          }
        }
      }
    }, ...dataRadialChart};
    //
    this.dataColumnChart = {
      ...{
        series: [{ name: 'Visitas', data: this.visitaPorMes }]
      }, ...dataColumnChart };
    //
    this.loaded = true;
  }

  private showSpinner(loading: boolean) {
    if (loading) {
      this.spinner.show('BASE_SPINNER');
      return;
    }

    this.spinner.hide('BASE_SPINNER');
  }

  get activosInactivosResidente() {
      const residente = this.service.data[0]?.residente;
      const inactivos = residente?.total - residente?.activos;
      return [residente?.activos, inactivos];
  }

  get tipoResidenciaLength(): number[] {
    const residencia = this.service.data[0]?.residencia;
    return [residencia?.casa, residencia?.otros, residencia?.negocio, residencia?.apartamento];
  }

  get visitaPorMes(): number[] {
    const visita = this.service.data[0]?.visita;
    return visita?.series as number[];
  }

  get pobladaPorciento(): number {
      const residencia = this.service.data[0]?.residencia;
      return residencia?.poblada / residencia?.total;
  }

  get residenteGenero(): { masculino: number, femenino: number } {
    const residente = this.service.data[0]?.residente;

    return {
      masculino: residente?.masculino / residente?.total,
      femenino: residente?.femenino / residente?.total
    };
  }

  get tipoResidentePorciento(): { propietario: number, inquilino: number, habitante: number, otros: number } {
    const residente = this.service.data[0]?.residente;

    return {
      propietario: residente?.propietario / residente?.total,
      inquilino: residente?.inquilino / residente?.total,
      habitante: residente?.habitante / residente?.total,
      otros: residente?.otros / residente?.total
    };
  }

  get residentePorMes(): { lastMonth: number, beforeLastMonth: number } {
    const residente = this.service.data[0]?.residente;
    return {
      lastMonth: residente?.lastMonth,
      beforeLastMonth: residente?.beforeLastMonth
    };
  }

  get residenciaPorMes(): { lastMonth: number, beforeLastMonth: number } {
    const residencia = this.service.data[0]?.residencia;
    return {
      lastMonth: residencia?.lastMonth,
      beforeLastMonth: residencia?.beforeLastMonth
    };
  }

  get residentePorCasa(): number {
    const residente = this.service.data[0]?.residente;
    return residente?.residentePorCasa / residente?.total;
  }

  get vehiculoPorCasa(): number {
    const vehiculo = this.service.data[0]?.vehiculo;
    return vehiculo?.vehiculoPorCasa / vehiculo?.total;
  }

  get visitaMejoras(): any | number {
    const visita = this.service.data[0]?.visita;
    const thisMonthPercent = ((visita?.lastMonth?.count - visita?.thisMonth?.count)
                              / visita?.lastMonth?.count) * -1;
    const lastMonthPercent = ((visita?.beforeLastMonth?.count - visita?.lastMonth?.count)
                              / visita?.beforeLastMonth?.count) * -1;
    return {
      thisMonth: isFinite(thisMonthPercent) ? thisMonthPercent : 0,
      lastMonth: isFinite(lastMonthPercent) ? lastMonthPercent : 0
    };
  }

  get ultimasVisitas(): IVisitaModel[] {
      const visita = this.service.data[0]?.visita;
      switch (this.model) {
        case 1: return visita?.thisMonth?.data as IVisitaModel[];

        case 2: return visita?.lastMonth?.data as IVisitaModel[];

        case 3: return visita?.beforeLastMonth?.data as IVisitaModel[];

        default: break;
      }

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

