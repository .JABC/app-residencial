import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Module
import { SharedModule } from '../shared/shared.module';
import { NgApexchartsModule  } from 'ng-apexcharts';
import { DashboardResidencialComponent } from './dashboard-residencial/dashboard-residencial.component';
import { DashboardPagosComponent } from './dashboard-pagos/dashboard-pagos.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardService } from './dashboard.service';

const routes: Routes =  [
  { path: '', redirectTo: 'residencial', pathMatch: 'full' },
  {
      path: '',
      component: DashboardComponent,
      children: [
        {
          path: 'residencial',
          component: DashboardResidencialComponent,
          data: { title: 'Dashboard Residencial' }
        },
        {
          path: 'pagos',
          component: DashboardPagosComponent,
          data: { title: 'Dashboard Pagos y Facturas' }
        }
      ]
  }
];


@NgModule({
  declarations: [
    DashboardComponent,
    DashboardResidencialComponent,
    DashboardPagosComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    NgApexchartsModule,
    RouterModule.forChild(routes)
  ],
  providers: [DashboardService]
})
export class DashboardModule { }
