import { Injectable } from '@angular/core';
import { ManagerDataService, ServiceState } from '@core/service/service.manager';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class DashboardService extends ManagerDataService<any> {

    constructor(public httpc: HttpClient) {
        super('dashboard');
    }

}

