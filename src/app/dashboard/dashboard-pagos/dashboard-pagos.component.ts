import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { AppTitle } from '@shared/tools';

@Component({
    selector: 'app-dashboard-pagos',
    templateUrl: './dashboard-pagos.component.html'
})
export class DashboardPagosComponent extends AppTitle implements OnInit {
    
    constructor(public service: DashboardService, public route: ActivatedRoute) {
        super(route);
    }

    ngOnInit(): void { }
}
