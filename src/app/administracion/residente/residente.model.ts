import { IEntityModel } from '@shared/model/entity.model';
import { IVehiculoModel } from '@adm/vehiculo/vehiculo.model';
import { IResidenciaModel } from '@adm/residencia/residencia.model';

export interface IResidenteModel extends IEntityModel {
    DNI?: number;
    Nombre?: string;
    Apellido?: string;
    NombreCompleto?: string;
    ResidenciaId?: number;
    ResidenciaNumero?: string;
    ResidenciaPropietarioNombreCompleto: string;
    TipoResidenteId?: number;
    TipoResidenteDescripcion?: string;
    ParentescoId?: number;
    ParentescoDescripcion?: string;
    Nacimiento?: Date;
    Telefono?: number;
    Genero?: string;
    Activo?: boolean;
    Vehiculos: IVehiculoModel[];

    Residencias: IResidenciaModel[]; // Propietario

}

export enum TipoResidente {
    Propietario = 1,
    Inquilino,
    Habitante
}

export enum Parentesco {
    Ninguno = 1,
    Hermano,
    Esposo,
    Primo,
    Sobrino,
    Padre,
    Hijo,
    Otro
}
