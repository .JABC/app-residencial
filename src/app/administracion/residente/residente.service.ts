import { Injectable } from '@angular/core';
import { ManagerDataService, ServiceState } from '@core/service/service.manager';
import { IResidenteModel, TipoResidente, Parentesco } from './residente.model';

@Injectable({ providedIn: 'root' })
export class ResidenteService extends ManagerDataService<IResidenteModel> {

  tipoResidente: any[] = [];
  parentesco: any[] = [];
  radioButtons = ['M', 'F'];

  constructor() {
    super('residente');
    this.setTipoResidenteItems();
    this.setParentescoItems();
  }

  private setTipoResidenteItems() {
    for (const item in TipoResidente) {
      if (isNaN(Number(item))) {
        this.tipoResidente.push({
          label: item,
          value: TipoResidente[item]
        });
      }
    }
  }

  private setParentescoItems() {
    for (const item in Parentesco) {
      if (isNaN(Number(item))) {
        this.parentesco.push({
          label: item,
          value: Parentesco[item]
        });
      }
    }
  }

  moverA(model: IResidenteModel) {
    this.onState$.next(ServiceState.Load);
    this.postMethod('moverA', model, (result: any) => {
      if (!result) {
        return;
      }

      this.load();
    });
  }

}
