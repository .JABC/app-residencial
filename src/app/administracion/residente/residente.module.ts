import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ResidenteListComponent } from './list/list.component';
import { ResidenteService } from './residente.service';
import { ResidenteComponent } from './residente.component';
import { ResidenteFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: ResidenteComponent,
        children: [
            {
                path: '',
                component: ResidenteListComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        ResidenteComponent,
        ResidenteListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ResidenteFormModule,
        RouterModule.forChild(routes)
    ]
})
export class ResidenteModule { }
