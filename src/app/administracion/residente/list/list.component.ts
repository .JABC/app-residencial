import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { IResidenteModel, TipoResidente } from '../residente.model';
import { ResidenteService } from '../residente.service';
import { ManagerDataList } from '@core/template/list/list.manager';
import { ResidenteFormComponent } from '../form/form.component';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html'
})
export class ResidenteListComponent extends ManagerDataList<IResidenteModel, ResidenteService>
  implements OnInit, OnDestroy, AfterViewInit {

  columns: SearchColumn[] = [
    new SearchColumn('Identificacion (DNI)', 'DNI'),
    new SearchColumn('Nombre', 'NombreCompleto'),
    new SearchColumn('Tipo', 'TipoResidenteDescripcion'),
    new SearchColumn('Numero de Residencia', 'ResidenciaNumero')
  ];

  constructor(public service: ResidenteService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

}
