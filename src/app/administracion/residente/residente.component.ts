import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';

@Component({
    selector: 'app-residente',
    templateUrl: './residente.component.html'
})
export class ResidenteComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }
}
