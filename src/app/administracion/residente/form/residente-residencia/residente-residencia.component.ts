import { Component, OnInit, Input } from '@angular/core';
import { Pagination } from '@core/service/service.model';
import { IResidenteModel } from '../../residente.model';
import { ResidenteService } from '../../residente.service';

@Component({
    selector: 'app-residente-residencia',
    templateUrl: './residente-residencia.component.html',
})
export class ResidenteResidenciaComponent implements OnInit {

    pagination = new Pagination(10, this.service.model.Residencias?.length);

    constructor(public service: ResidenteService) { }

    ngOnInit() {

    }
}
