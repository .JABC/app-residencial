import { Component, OnInit, OnDestroy } from '@angular/core';
import { ManagerDataForm } from '@core/template/form/form.manager';
import { IResidenteModel } from '../residente.model';
import { ResidenteService } from '../residente.service';

@Component({
    selector: 'app-residente-form',
    templateUrl: './form.component.html'
})
export class ResidenteFormComponent extends ManagerDataForm<IResidenteModel, ResidenteService>
  implements OnInit, OnDestroy {

  activeId = 1;

  constructor(public service: ResidenteService) {
    super(service);
  }

  ngOnInit() {
    // super.ngOnInit();
  }

  ngOnDestroy() {
    // super.ngOnDestroy();
  }

}
