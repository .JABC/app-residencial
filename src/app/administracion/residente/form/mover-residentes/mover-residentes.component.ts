import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { ResidenteService } from '@adm/residente/residente.service';
import { IResidenteModel } from '@adm/residente/residente.model';
import { Filter, EFilterOperator } from '@core/service/service.model';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-mover-residentes',
    templateUrl: './mover-residentes.component.html',
    providers: [ResidenciaService, ResidenteService],
})
export class MoverResidentesComponent implements OnInit, OnDestroy {

    @Input() residente: IResidenteModel;
    // tslint:disable-next-line
    @Output() onLoad = new EventEmitter();

    filter: Filter[] = [];

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'UniqueIdentifier'),
        new SearchColumn('Numero'),
        new SearchColumn('Tipo', 'TipoResidenciaDescripcion'),
        new SearchColumn('Piso')
    ];

    constructor(
        public service: ResidenteService,
        public rsService: ResidenciaService
    ) { }

    ngOnInit() {
        this.service.subscribe();
        this.rsService.subscribe();
        this.filter.push(new Filter('Numero', this.residente?.ResidenciaNumero, EFilterOperator.NotEquals));
    }

    selectedItem(item: any) {
        this.residente.ResidenciaNumero = item.Numero;
        this.service.moverA(this.residente);
        this.service.onLoad.subscribe(() => this.onLoad.emit());
    }

    close() {
        this.service.modal.closeModal();
    }

    ngOnDestroy() {
        this.service.unsubscribe();
        this.rsService.unsubscribe();
    }

}
