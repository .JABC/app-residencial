import { Component, OnInit, OnDestroy } from '@angular/core';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { IResidenteModel } from '@adm/residente/residente.model';
import { Subscription } from 'rxjs';
import { ResidenteService } from '@adm/residente/residente.service';
import { Filter, EFilterOperator } from '@core/service/service.model';

@Component({
    selector: 'app-residente-relacion',
    templateUrl: './residente-relacion.component.html',
    providers: [ResidenciaService]
})
export class ResidenteRelacionComponent implements OnInit, OnDestroy {

    data: IResidenteModel[] = [];

    private subscription: Subscription;

    constructor(
        public service: ResidenteService,
        public residenciaService: ResidenciaService) { }

    ngOnInit() {
        this.residenciaService.subscribe();
        const newFilter = new Filter('Id', String(this.service.model.ResidenciaId), EFilterOperator.Equals);
        this.residenciaService.config.Filters = [newFilter];
        this.residenciaService.load();
        this.subscription = this.residenciaService.onLoad.subscribe(
            (data: any[]) => this.data = data[0]?.Residentes
        );
    }

    moveToOnLoad() {
        this.service.load();
        this.service.modal.closeModal();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.residenciaService.unsubscribe();
    }
}
