import { NgModule } from '@angular/core';
import { ResidenteFormComponent } from './form.component';
import { ResidenteResidenciaComponent } from './residente-residencia/residente-residencia.component';
import { ResidenteRelacionComponent } from './residente-relacion/residente-relacion.component';
import { MoverResidentesComponent } from './mover-residentes/mover-residentes.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ResidenteService } from '../residente.service';

@NgModule({
    declarations: [
        ResidenteFormComponent,
        ResidenteResidenciaComponent,
        ResidenteRelacionComponent,
        MoverResidentesComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        ResidenteFormComponent,
        ResidenteResidenciaComponent,
        ResidenteRelacionComponent,
        MoverResidentesComponent
    ],
    providers: [ResidenteService]
})
export class ResidenteFormModule { }
