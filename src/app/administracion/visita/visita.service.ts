import { Injectable } from '@angular/core';
import { ManagerDataService } from '@core/service/service.manager';
import { IVisitaModel } from './visita.model';

@Injectable({ providedIn: 'root' })
export class VisitaService extends ManagerDataService<IVisitaModel> {

    constructor() {
        super('visita');
    }

}
