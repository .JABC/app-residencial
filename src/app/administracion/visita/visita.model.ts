import { IEntityModel } from '@shared/model/entity.model';

export interface IVisitaModel extends IEntityModel {
    VisitanteNombre?: string;
    VisitanteDNI?: string;
    Motivo?: string;
    Llegada?: Date;
    Salida?: Date;
    Nota?: string;
    Reportado?: boolean;
    DarEntrada?: Date;
    TieneEntrada?: boolean;
    ResidenciaId?: number;
    ResidenciaNumero?: string;
}
