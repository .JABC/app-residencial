import { Component, OnInit, OnDestroy } from '@angular/core';
import { ManagerDataList } from '@core/template/list/list.manager';
import { VisitaService } from '../visita.service';
import { IVisitaModel } from '../visita.model';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-visita-list',
    templateUrl: './list.component.html'
})
export class VisitaListComponent extends ManagerDataList<IVisitaModel, VisitaService>
    implements OnInit, OnDestroy {

    columns: SearchColumn[] = [
        new SearchColumn('Nombre', 'VisitanteNombre'),
        new SearchColumn('Identificacion (DNI)', 'VisitanteDNI'),
        new SearchColumn('Numero de Residencia', 'ResidenciaNumero')
    ];

    constructor(public service: VisitaService) {
        super(service);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }

}
