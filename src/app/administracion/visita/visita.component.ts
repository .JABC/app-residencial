import { Component } from '@angular/core';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-visita',
    templateUrl: './visita.component.html'
})
export class VisitaComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
