import { Component, OnInit, ViewChild } from '@angular/core';
import { VisitaService } from '../../visita.service';
import { TypeModal } from '@shared/modal/modal.model';
import { SearchColumn } from '@core/template/search/search.component';
import { ResidenciaService } from '@adm/residencia/residencia.service';

@Component({
    selector: 'app-visita-residencia',
    templateUrl: './visita-residencia.component.html'
})
export class VisitaResidenciaComponent implements OnInit {

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'UniqueIdentifier'),
        new SearchColumn('Numero'),
        new SearchColumn('Tipo', 'TipoResidenciaDescripcion'),
        new SearchColumn('Piso')
    ];

    constructor(
        public service: ResidenciaService,
        public visitaService: VisitaService
    ) { }

    ngOnInit() {
        this.service.subscribe();
    }

    selectedItem(item: any) {
        this.visitaService.model.ResidenciaId = item.Id;
        this.visitaService.model.ResidenciaNumero = item.Numero;
    }
}
