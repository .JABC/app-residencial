import { Component } from '@angular/core';
import { ManagerDataForm } from '@core/template/form/form.manager';
import { IVisitaModel } from '../visita.model';
import { VisitaService } from '../visita.service';

@Component({
    selector: 'app-visita-form',
    templateUrl: './form.component.html'
})
export class VisitaFormComponent extends ManagerDataForm<IVisitaModel, VisitaService> {

    constructor(public service: VisitaService) {
        super(service);
    }


}
