import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { VisitaFormComponent } from './form.component';
import { VisitaService } from '../visita.service';
import { VisitaResidenciaComponent } from './visita-residencia/visita-residencia.component';
import { ResidenciaFormModule } from '@adm/residencia/form/form.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ResidenciaFormModule
    ],
    declarations: [
        VisitaFormComponent,
        VisitaResidenciaComponent
    ],
    exports: [
        VisitaFormComponent,
        ResidenciaFormModule
    ],
    providers: [VisitaService]
})
export class VisitaFormModule { }
