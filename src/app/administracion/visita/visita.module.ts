import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { VisitaComponent } from './visita.component';
import { VisitaListComponent } from './list/list.component';
import { VisitaFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: VisitaComponent,
        children: [{ path: '', component: VisitaListComponent }],
        data : { title: 'Acceso Visita' }
    }
];

@NgModule({
    declarations: [
        VisitaComponent,
        VisitaListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        VisitaFormModule,
        RouterModule.forChild(routes)
    ],
})
export class VisitaModule { }
