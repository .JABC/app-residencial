import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ResidenciaListComponent } from './list/list.component';
import { ResidenciaComponent } from './residencia.component';
import { CalleFormModule } from '../calle/form/form.module';
import { ResidenciaFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: ResidenciaComponent,
        children: [
            {
                path: '',
                component: ResidenciaListComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        ResidenciaComponent,
        ResidenciaListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ResidenciaFormModule,
        CalleFormModule,
        RouterModule.forChild(routes)
    ]
})
export class ResidenciaModule { }
