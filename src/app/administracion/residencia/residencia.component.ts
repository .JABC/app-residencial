import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';
import { ResidenciaService } from './residencia.service';

@Component({
    selector: 'app-residencia',
    templateUrl: './residencia.component.html'
})
export class ResidenciaComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
