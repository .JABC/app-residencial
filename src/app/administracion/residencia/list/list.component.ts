import { Component, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { IResidenciaModel } from '../residencia.model';
import { ResidenciaService } from '../residencia.service';
import { ManagerDataList } from '@core/template/list/list.manager';
import { ResidenciaFormComponent } from '../form/form.component';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html'
})
export class ResidenciaListComponent extends ManagerDataList<IResidenciaModel, ResidenciaService>
  implements OnInit, OnDestroy, AfterViewInit {

  columns: SearchColumn[] = [
    new SearchColumn('Numero'),
    new SearchColumn('Tipo', 'TipoResidenciaDescripcion'),
    new SearchColumn('Piso')
  ];

  constructor(public service: ResidenciaService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

}
