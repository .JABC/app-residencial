import { Injectable } from '@angular/core';
import { IResidenciaModel, ETipoResidencia } from './residencia.model';
import { ManagerDataService } from '@core/service/service.manager';
import { IResidenteModel } from '../residente/residente.model';

@Injectable({
    providedIn: 'root'
})
export class ResidenciaService extends ManagerDataService<IResidenciaModel> {
    tipoResidencia: any[] = [];

    constructor() {
      super('residencia');
      this.setTipoResidenciaItems();
    }

    setTipoResidenciaItems() {
      for (const item in ETipoResidencia) {
        if (isNaN(Number(item))) {
          this.tipoResidencia.push(
            {label: item, value: ETipoResidencia[item]}
          );
        }
      }
    }

}
