import { IEntityModel } from '@shared/model/entity.model';
import { IResidenteModel } from '@adm/residente/residente.model';
import { IVisitaModel } from '@adm/visita/visita.model';

export interface IResidenciaModel extends IEntityModel {
    UniqueIdentifier: string;
    Numero?: string;
    PropietarioId?: number;
    PropietarioNombreCompleto?: string;
    PropietarioDNI?: string;
    CalleId?: number;
    CalleNombre?: string;
    TipoResidenciaId?: number;
    TipoResidenciaDescripcion?: string;
    Piso?: number;

    Residentes?: IResidenteModel[];
    Visitas?: IVisitaModel[];
}

export enum ETipoResidencia {
    Apartamento = 1,
    Casa,
    Negocio
}
