import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SearchColumn } from '@core/template/search/search.component';
import { Subscription } from 'rxjs';
import { CalleService } from '@adm/calle/calle.service';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { TypeModal } from '@shared/modal/modal.model';

@Component({
    selector: 'app-residencia-calle',
    templateUrl: './residencia-calle.component.html'
})
export class ResidenciaCalleComponent implements OnInit, OnDestroy {

    @ViewChild('form') form: any;

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'Id'),
        new SearchColumn('Nombre')
    ];

    private subscription: Subscription;

    constructor(
        public service: CalleService,
        public residenciaService: ResidenciaService) {}


    ngOnInit() {
        this.service.subscribe();
        this.subscription = this.service.onAdded.subscribe(
            (model: any) => this.selectedItem(model));
        this.subscription.add(this.residenciaService.onAdded.subscribe(
            () => this.service.model = {}
        ));
    }

    add() {
        this.service.modal.showModal(this.form, { type: TypeModal.Data });
    }

    selectedItem(item: any) {
        this.service.model = item;
        this.residenciaService.model.CalleId = item.Id;
    }

    ngOnDestroy() {
        this.service.model = {};
        this.residenciaService.model = {};
        this.service.unsubscribe();
        this.subscription.unsubscribe();
        this.subscription = null;
    }

}
