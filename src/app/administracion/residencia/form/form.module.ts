import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ResidenteFormModule } from '@adm/residente/form/form.module';
import { CalleFormModule } from '@adm/calle/form/form.module';
import { VehiculoFormModule } from '@adm/vehiculo/form/form.module';
import { ResidenciaCalleComponent } from './residencia-calle/residencia-calle.component';
import { ResidenciaPropietarioComponent } from './residencia-propietario/residencia-propietario.component';
import { ResidenciaResidenteComponent } from './residencia-residente/residencia-residente.component';
import { ResidenciaFormComponent } from './form.component';
import { ResidenciaVehiculoComponent } from './residencia-vehiculo/residencia-vehiculo.component';
import { ResidenciaService } from '../residencia.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ResidenteFormModule,
        CalleFormModule,
        VehiculoFormModule
    ],
    declarations: [
        ResidenciaCalleComponent,
        ResidenciaPropietarioComponent,
        ResidenciaResidenteComponent,
        ResidenciaFormComponent,
        ResidenciaVehiculoComponent
    ],
    exports: [
        ResidenciaCalleComponent,
        ResidenciaPropietarioComponent,
        ResidenciaResidenteComponent,
        ResidenciaFormComponent,
        ResidenciaVehiculoComponent
    ],
    providers: [ResidenciaService]
})
export class ResidenciaFormModule { }
