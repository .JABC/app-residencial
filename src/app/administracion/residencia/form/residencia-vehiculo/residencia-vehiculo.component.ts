import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Pagination } from '@core/service/service.model';
import { IResidenteModel } from '@adm/residente/residente.model';

@Component({
    selector: 'app-residencia-vehiculo',
    templateUrl: './residencia-vehiculo.component.html',

})
export class ResidenciaVehiculoComponent implements OnInit, OnDestroy {

    @Input() residentes: IResidenteModel[];

    vehiculos: any[] = [];
    pagination = new Pagination(10);

    constructor() { }

    ngOnInit() {
        this.residentes?.forEach((value) => this.vehiculos.push(value.Vehiculos));
        this.vehiculos = this.vehiculos.reduce((acc, value) => acc?.concat(value), []);
        this.pagination.TotalCount = this.vehiculos?.length;
    }

    ngOnDestroy() {

    }

}
