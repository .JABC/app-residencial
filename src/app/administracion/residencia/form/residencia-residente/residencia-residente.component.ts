import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Filter, EFilterOperator, Pagination } from '@core/service/service.model';
import { SearchColumn } from '@core/template/search/search.component';
import { Subscription, EMPTY } from 'rxjs';
import { ResidenteService } from '@adm/residente/residente.service';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { TypeModal } from '@shared/modal/modal.model';
import { ModalComponent } from '@shared/modal/modal.component';
import { TipoResidente } from '@adm/residente/residente.model';

@Component({
    selector: 'app-residencia-residente',
    templateUrl: './residencia-residente.component.html'
})
export class ResidenciaResidenteComponent implements OnInit, OnDestroy {

    @ViewChild('form') form: any;

    filter: Filter[] = [
        new Filter('ResidenciaId', 'null', EFilterOperator.Equals)
    ];
    // residentes: any[] = [];
    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'Id'),
        new SearchColumn('DNI'),
        new SearchColumn('Nombre', 'NombreCompleto')
    ];

    pagination = new Pagination(10, this.residenciaService.model.Residentes?.length);

    subscription: Subscription;

    constructor(
        public service: ResidenteService,
        public residenciaService: ResidenciaService) { }

    ngOnInit() {
        this.service.subscribe();
        this.subscription = this.service.onAdded.subscribe(
            (model: any) => this.selectedItem(model)
        );
    }

    add() {
        this.service.modal.showModal(this.form, { type: TypeModal.Data });
    }

    // edit(model: any) {
    //     this.service.onEdit(model);
    //     this.service.modal.showModal(this.form, { type: TypeModal.Data });
    // }

    delete(model: any) {
        const modalConfig = {
            type: TypeModal.Delete,
            message: 'El registro solo sera eliminado de esta residencia'
        };
        this.service.modal.showModal(ModalComponent, modalConfig)
            .then(() => {
                const index = this.residenciaService.model.Residentes.indexOf(model);
                this.residenciaService.model.Residentes.splice(index, 1);
                model.ResidenciaId = null;
                model.ParentescoId = null;
                this.service.model = model;
                this.service.postMethod('update', this.service.model);
                this.service.model = {};
            })
            .catch(() => EMPTY);
    }

    selectedItem(item: any) {
        item.ResidenciaId = this.residenciaService.model.Id;
        item.TipoResidenteDescripcion = TipoResidente[item.TipoResidenteId];
        this.residenciaService.model.Residentes.push(item);
        this.service.model = item;
        this.service.postMethod('update', this.service.model);
        this.service.model = {};
    }

    setParentesco(model: any) {
        this.service.postMethod('update', model);
    }

    moveToOnLoad() {
        this.residenciaService.load();
        this.residenciaService.modal.closeModal();
    }

    get residente() {
        const residentes = this.residenciaService.model?.Residentes;
        return residentes !== undefined ? residentes[0] : null;
    }

    ngOnDestroy() {
        this.service.unsubscribe();
        this.subscription.unsubscribe();
        this.subscription = null;
    }

}
