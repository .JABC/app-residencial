import { Component, OnInit, OnDestroy } from '@angular/core';
import { IResidenciaModel } from '../residencia.model';
import { ResidenciaService } from '../residencia.service';
import { ManagerDataForm } from '@core/template/form/form.manager';

@Component({
    selector: 'app-residencia-form',
    templateUrl: './form.component.html'
})
export class ResidenciaFormComponent extends ManagerDataForm<IResidenciaModel, ResidenciaService>
  implements OnInit, OnDestroy {

  constructor(public service: ResidenciaService) {
    super(service);
  }

  ngOnInit() {
    // super.ngOnInit();
  }

  ngOnDestroy() {
    // super.ngOnDestroy();
  }

}
