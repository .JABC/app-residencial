import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Filter, EFilterOperator } from '@core/service/service.model';
import { SearchColumn } from '@core/template/search/search.component';
import { Subscription } from 'rxjs';
import { ResidenteService } from '@adm/residente/residente.service';
import { ResidenciaService } from '@adm/residencia/residencia.service';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';

@Component({
    // tslint:disable-next-line
    selector: 'app-residencia-propietario',
    templateUrl: './residencia-propietario.component.html'
})
export class ResidenciaPropietarioComponent implements OnInit, OnDestroy {

    @ViewChild('form') form: any;

    filter: Filter[] = [
        new Filter('TipoResidenteId', '1', EFilterOperator.Equals)
    ];

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'Id'),
        new SearchColumn('DNI'),
        new SearchColumn('Nombre', 'NombreCompleto')
    ];

    private subscription = new Subscription();

    constructor(
        public service: ResidenteService,
        public residenciaService: ResidenciaService,
        public modal: ModalService) {}

    ngOnInit() {
        this.subscription.add(this.service.onAdded.subscribe(
            (model: any) => this.selectedItem(model)));
        this.subscription.add(this.residenciaService.onAdded.subscribe(
            () => this.service.model = {}
        ));
    }

    add() {
        this.service.model.TipoResidenteId = 1;
        this.service.modal.showModal(this.form, { type: TypeModal.Data });
    }

    selectedItem(item: any) {
        this.service.model = item;
        this.residenciaService.model.PropietarioId = item.Id;
    }

    ngOnDestroy() {
        this.service.model = {};
        this.residenciaService.model = {};
        this.subscription.unsubscribe();
        this.subscription = null;
    }

}
