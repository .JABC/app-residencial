import { Component, OnInit } from '@angular/core';
import { AreaComunService } from '../area-comun.service';
import { ManagerDataForm } from '@core/template/form/form.manager';
import { IAreaComunModel } from '../area-comun.model';

@Component({
    selector: 'app-area-comun-form',
    templateUrl: './form.component.html',
})
export class AreaComunFormComponent extends ManagerDataForm<IAreaComunModel, AreaComunService>
    implements OnInit {

    constructor(public service: AreaComunService) {
        super(service);
    }

    ngOnInit(): void { }
}
