import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AreaComunFormComponent } from './form.component';
import { SharedModule } from '@shared/shared.module';
import { AreaComunService } from '../area-comun.service';

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [AreaComunFormComponent],
    exports: [AreaComunFormComponent],
    providers: [AreaComunService]
})
export class AreaComunFormModule { }
