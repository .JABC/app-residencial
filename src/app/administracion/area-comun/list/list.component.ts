import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { AreaComunService } from '../area-comun.service';
import { ManagerDataList } from '@core/template/list/list.manager';
import { IAreaComunModel } from '../area-comun.model';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
})
export class AreaComunListComponent extends ManagerDataList<IAreaComunModel, AreaComunService>
  implements OnInit, OnDestroy, AfterViewInit {

    columns: SearchColumn[] = [
        new SearchColumn('Nombre'),
        new SearchColumn('Precio')
    ];

    constructor(public service: AreaComunService) {
        super(service);
    }

    ngOnInit(): void { }
}
