import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared/shared.module';
import { AreaComunListComponent } from './list/list.component';
import { AreaComunComponent } from './area-comun.component';
import { AreaComunFormModule } from './form/form.module';
import { AreaComunService } from './area-comun.service';

const routes: Routes = [
    {
        path: '',
        component: AreaComunComponent,
        children: [{ path: '', component: AreaComunListComponent }]
    }
];

@NgModule({
    declarations: [
        AreaComunComponent,
        AreaComunListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        AreaComunFormModule,
        RouterModule.forChild(routes)
    ]
})
export class AreaComunModule { }
