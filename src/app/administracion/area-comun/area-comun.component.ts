import { Component, OnInit } from '@angular/core';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-area-comun',
    templateUrl: './area-comun.component.html'
})
export class AreaComunComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
