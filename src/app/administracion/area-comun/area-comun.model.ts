import { IEntityModel } from '@shared/model/entity.model';

export interface IAreaComunModel extends IEntityModel {
    Nombre: string;
    Descripcion: string;
    Precio: number;
    Estado: string;
}
