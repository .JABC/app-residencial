import { Injectable } from '@angular/core';
import { ManagerDataService } from '@core/service/service.manager';
import { IAreaComunModel } from './area-comun.model';

@Injectable({ providedIn: 'root' })
export class AreaComunService extends ManagerDataService<IAreaComunModel> {

    constructor() {
        super('areaComun');
    }

}
