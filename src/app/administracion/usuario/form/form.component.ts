import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { IUsuarioModel } from '../usuario.model';
import { ManagerDataForm } from '@core/template/form/form.manager';

@Component({
    selector: 'app-usuario-form',
    templateUrl: './form.component.html'
})
export class UsuarioFormComponent extends ManagerDataForm<IUsuarioModel, UsuarioService>
    implements OnInit {

    constructor(public service: UsuarioService) {
        super(service);
    }

    ngOnInit(): void { }
}
