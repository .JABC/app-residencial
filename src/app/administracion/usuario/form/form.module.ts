import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioFormComponent } from './form.component';
import { UsuarioService } from '../usuario.service';
import { SharedModule } from '@shared/shared.module';

@NgModule({
    declarations: [UsuarioFormComponent],
    imports: [CommonModule, SharedModule],
    exports: [UsuarioFormComponent],
    providers: [UsuarioService]
})
export class UsuarioFormModule { }
