import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuarioFormModule } from './form/form.module';
import { UsuarioListComponent } from './list/list.component';
import { UsuarioComponent } from './usuario.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '@shared/shared.module';

const routes: Routes = [
    {
        path: '',
        component: UsuarioComponent,
        children: [
            {
                path: '',
                component: UsuarioListComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        UsuarioComponent,
        UsuarioListComponent
    ],
    imports: [
        CommonModule,
        UsuarioFormModule,
        SharedModule,
        RouterModule.forChild(routes)
    ]
})
export class UsuarioModule { }
