import { Injectable } from '@angular/core';
import { ManagerDataService } from '@core/service/service.manager';
import { IUsuarioModel, TipoUsuario } from './usuario.model';

@Injectable({
    providedIn: 'root'
})
export class UsuarioService extends ManagerDataService<IUsuarioModel> {

    tipoUsuario: any[] = [];

    constructor() {
        super('user');
        this.setTipoUsuarioItems();
    }

    private setTipoUsuarioItems() {
        for (const item in TipoUsuario) {
          if (isNaN(Number(item))) {
            this.tipoUsuario.push({
              label: item,
              value: TipoUsuario[item]
            });
          }
        }
      }

}
