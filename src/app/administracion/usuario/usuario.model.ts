import { IEntityModel } from '@shared/model/entity.model';

export interface IUsuarioModel extends IEntityModel {
    UserName: string;
    Email: string;
    IsLocked: string;
    RoleId: number;
    RoleName: string;
}

export enum TipoUsuario {
    Administrador = 1,
    Guardia,
    Residente
}

