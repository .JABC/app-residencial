import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { UsuarioService } from '../usuario.service';
import { ManagerDataList } from '@core/template/list/list.manager';
import { IUsuarioModel } from '../usuario.model';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-usuario-list',
    templateUrl: './list.component.html'
})
export class UsuarioListComponent extends ManagerDataList<IUsuarioModel, UsuarioService>
    implements OnInit, AfterViewInit, OnDestroy {

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'Id'),
        new SearchColumn('Nombre de Usuario', 'Username'),
	new SearchColumn('Correo', 'Email'),
        new SearchColumn('Tipo', 'RoleName')
    ];

    constructor(public service: UsuarioService) {
        super(service);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngAfterViewInit() {
        super.ngAfterViewInit();
    }

    ngOnDestroy() {
        super.ngOnDestroy();
    }

}
