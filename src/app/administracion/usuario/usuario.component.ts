import { Component } from '@angular/core';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-usuario',
    templateUrl: './usuario.component.html'
})
export class UsuarioComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
