import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'residente', pathMatch: 'full' },
  {
    path: '',
    children: [
      {
        path: 'residente',
        loadChildren: () => import('./residente/residente.module')
                            .then(m => m.ResidenteModule),
        data: { title: 'Adminitrar Residentes' }
      },
      {
        path: 'residencia',
        loadChildren: () => import('./residencia/residencia.module')
                            .then(m => m.ResidenciaModule),
        data: { title: 'Adminitrar Residencias' }
      },
      {
        path: 'calle',
        loadChildren: () => import('./calle/calle.module')
                            .then(m => m.CalleModule),
        data: { title: 'Adminitrar Calles' }
      },
      {
        path: 'vehiculo',
        loadChildren: () => import('./vehiculo/vehiculo.module')
                            .then(m => m.VehiculoModule),
        data: { title: 'Adminitrar Vehiculos' }
      },
      {
        path: 'visita',
        loadChildren: () => import('./visita/visita.module')
                            .then(m => m.VisitaModule),
        data: { title: 'Adminitrar Visitas' }
      },
      {
        path: 'area-comun',
        loadChildren: () => import('./area-comun/area-comun.module')
                            .then(m => m.AreaComunModule),
        data: { title: 'Adminitrar Areas Comunes' }
      },
      {
        path: 'usuario',
        loadChildren: () => import('./usuario/usuario.module')
                            .then(m => m.UsuarioModule),
        data: { title: 'Adminitrar Usuarios' }
      }
    ]
  }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ]
})
export class AdminitracionModule { }
