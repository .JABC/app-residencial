import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ManagerDataList } from '@core/template/list/list.manager';
import { ICalleModel } from '../calle.model';
import { CalleService } from '../calle.service';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html'
})
export class CalleListComponent extends ManagerDataList<ICalleModel, CalleService>
  implements OnInit, OnDestroy, AfterViewInit {

  columns: SearchColumn[] = [
    new SearchColumn('Nombre'),
    new SearchColumn('Distancia')
  ];

  constructor(public service: CalleService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

}
