import { IEntityModel } from '@shared/model/entity.model';
import { IResidenciaModel } from '../residencia/residencia.model';

export interface ICalleModel extends IEntityModel {
    Nombre?: string;
    Distancia?: string;
    Residencias?: IResidenciaModel[];
}
