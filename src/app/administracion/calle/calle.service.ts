import { Injectable } from '@angular/core';
import { ICalleModel } from './calle.model';
import { ManagerDataService } from '@core/service/service.manager';

@Injectable({
    providedIn: 'root'
})
export class CalleService extends ManagerDataService<ICalleModel> {

    constructor() {
        super('calle');
    }

}

