import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';

@Component({
    selector: 'app-calle',
    templateUrl: './calle.component.html'
})
export class CalleComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}

