import { Component, OnInit, Input } from '@angular/core';
import { Pagination } from '@core/service/service.model';
import { CalleService } from '../../calle.service';

@Component({
    selector: 'app-calle-residencia',
    templateUrl: './calle-residencia.component.html'
})
export class CalleResidenciaComponent implements OnInit {

    pagination = new Pagination(10, this.service.model.Residencias?.length);

    constructor(public service: CalleService) { }

    ngOnInit() {

    }

}
