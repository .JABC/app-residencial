import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CalleFormComponent } from './form.component';
import { SharedModule } from '@shared/shared.module';
import { CalleResidenciaComponent } from './calle-residencia/calle-residencia.component';
import { CalleService } from '../calle.service';

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [CalleFormComponent, CalleResidenciaComponent],
    exports: [CalleFormComponent, CalleResidenciaComponent],
    providers: [CalleService]
})
export class CalleFormModule { }
