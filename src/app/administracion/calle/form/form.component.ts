import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ICalleModel } from '../calle.model';
import { CalleService } from '../calle.service';
import { ManagerDataForm } from '@core/template/form/form.manager';

@Component({
    selector: 'app-calle-form',
    templateUrl: './form.component.html'
})
export class CalleFormComponent extends ManagerDataForm<ICalleModel, CalleService>
  implements OnInit, OnDestroy {

  constructor(public service: CalleService) {
    super(service);
  }

  ngOnInit() {
    // super.ngOnInit();
  }

  ngOnDestroy() {
    // super.ngOnDestroy();
  }

}

