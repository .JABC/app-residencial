import { Routes, RouterModule } from '@angular/router';
import { CalleComponent } from './calle.component';
import { CalleListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { CalleFormModule } from './form/form.module';
import { CalleService } from './calle.service';

const routes: Routes = [
    {
        path: '',
        component: CalleComponent,
        children: [{ path: '', component: CalleListComponent }]
    }
];

@NgModule({
    declarations: [
        CalleComponent,
        CalleListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        CalleFormModule,
        RouterModule.forChild(routes)
    ]
})
export class CalleModule { }
