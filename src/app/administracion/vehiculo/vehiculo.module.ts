import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { VehiculoListComponent } from './list/list.component';
import { VehiculoComponent } from './vehiculo.component';
import { VehiculoFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: VehiculoComponent,
        children: [
            {
                path: '',
                component: VehiculoListComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        VehiculoComponent,
        VehiculoListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        VehiculoFormModule,
        RouterModule.forChild(routes)
    ]
})
export class VehiculoModule { }
