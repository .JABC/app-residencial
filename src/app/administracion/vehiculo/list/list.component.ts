import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { IVehiculoModel } from '../vehiculo.model';
import { VehiculoService } from '../vehiculo.service';
import { ManagerDataList } from '@core/template/list/list.manager';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html'
})
export class VehiculoListComponent extends ManagerDataList<IVehiculoModel, VehiculoService>
  implements OnInit, OnDestroy, AfterViewInit {

  columns: SearchColumn[] = [
    new SearchColumn('Matricula'),
    new SearchColumn('Descripcion'),
    new SearchColumn('Numero de Residencia', 'ResidenciaNumero')
  ];

  constructor(public service: VehiculoService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

}
