import { Component } from '@angular/core';
import { IVehiculoModel } from '../vehiculo.model';
import { VehiculoService } from '../vehiculo.service';
import { ManagerDataForm } from '@core/template/form/form.manager';

@Component({
    selector: 'app-vehiculo-form',
    templateUrl: './form.component.html'
})
export class VehiculoFormComponent extends ManagerDataForm<IVehiculoModel, VehiculoService> {

  constructor(public service: VehiculoService) {
    super(service);
  }

}
