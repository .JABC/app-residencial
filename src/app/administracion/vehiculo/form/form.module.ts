import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { VehiculoFormComponent } from './form.component';
import { VehiculoService } from '../vehiculo.service';
import { VehiculoResidenteComponent } from './vehiculo-residente/vehiculo-residente.component';
import { ResidenteFormModule } from '@adm/residente/form/form.module';

@NgModule({
    declarations: [
        VehiculoFormComponent,
        VehiculoResidenteComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ResidenteFormModule
    ],
    exports: [
        VehiculoFormComponent
    ],
    providers: [VehiculoService]
})
export class VehiculoFormModule { }
