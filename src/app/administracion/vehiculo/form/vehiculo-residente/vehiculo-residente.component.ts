import { Component, OnInit } from '@angular/core';
import { SearchColumn } from '@core/template/search/search.component';
import { ResidenteService } from '@adm/residente/residente.service';
import { VehiculoService } from '@adm/vehiculo/vehiculo.service';

@Component({
    selector: 'app-vehiculo-residente',
    templateUrl: './vehiculo-residente.component.html'
})
export class VehiculoResidenteComponent implements OnInit {

    columns: SearchColumn[] = [
        new SearchColumn('Codigo', 'Id'),
        new SearchColumn('Nombre', 'NombreCompleto'),
        new SearchColumn('Tipo', 'TipoResidenteDescripcion')
    ];

    constructor(
        public service: ResidenteService,
        public vehiculoService: VehiculoService
    ) { }

    ngOnInit() {
        this.service.subscribe();
    }

    selectedItem(item: any) {
        this.vehiculoService.model.ResidenteId = item.Id;
        this.vehiculoService.model.ResidenteNombreCompleto = item.NombreCompleto;
    }
}
