import { IEntityModel } from '@shared/model/entity.model';

export interface IVehiculoModel extends IEntityModel {
    ResidenteId?: number;
    ResidenteNombreCompleto?: string;
    Matricula?: string;
    Descripcion?: string;
}
