import { Injectable } from '@angular/core';
import { IVehiculoModel } from './vehiculo.model';
import { ManagerDataService } from '@core/service/service.manager';

@Injectable({
    providedIn: 'root'
})
export class VehiculoService extends ManagerDataService<IVehiculoModel> {

    constructor() {
      super('vehiculo');
    }

}
