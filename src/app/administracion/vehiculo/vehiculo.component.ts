import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';
import { VehiculoService } from './vehiculo.service';

@Component({
    selector: 'app-vehiculo',
    templateUrl: './vehiculo.component.html'
})
export class VehiculoComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
