import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormTemplateComponent } from './template/form/form.component';
import { InputWrapperComponent } from './template/input-wrapper/input-wrapper.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ListTemplateComponent } from './template/list/list.component';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from './template/search-bar/search-bar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { SearchTemplateComponent } from './template/search/search.component';
import { PaginationComponent } from './template/pagination/pagination.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [
        FormTemplateComponent,
        InputWrapperComponent,
        ListTemplateComponent,
        SearchBarComponent,
        SearchTemplateComponent,
        PaginationComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NgxSpinnerModule,
        NgbPaginationModule
    ],
    exports: [
        FormTemplateComponent,
        InputWrapperComponent,
        ListTemplateComponent,
        SearchBarComponent,
        SearchTemplateComponent,
        PaginationComponent
    ]
})
export class CoreModule { }
