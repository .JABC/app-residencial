// import { Subscription, Subject } from 'rxjs';
// import { AfterViewInit } from '@angular/core';

// // tslint:disable
// export class InputWrapper implements AfterViewInit {

//     protected _changes$: Subject<any> = new Subject();
//     protected _subscription: Subscription;

//     msgError = 'Este campo es requerido';

//     constructor() { }

//     ngAfterViewInit() {
//         this._subscription = this._changes$.subscribe(
//             (inputErrors) => this.onError(inputErrors),
//             error => console.error(error),
//             () => this._subscription.unsubscribe()
//         );
//     }

//     onError(errors: any) {
//         if (!errors) {
//             return;
//         }

//         for (const error in errors) {
//             switch (error) {
//                 case 'minlength':
//                     const minLength = errors[error]?.requiredLength;
//                     this.msgError = `Minimo ${minLength} caracteres`;
//                     break;

//                 case 'pattern':
//                     this.msgError = 'Debe seguir el patron'
//                     break;

//                 default:
//                     this.msgError = 'Este campo es requerido'
//                     break;
//             }
//         }
//     }

// }

