import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

// tslint:disable
@Component({
    selector: '[input-wrapper]',
    templateUrl: './input-wrapper.component.html'
})
export class InputWrapperComponent {

    @Input() label: string;
    @Input() labelClass: string;
    @Input() errorMsg = 'Este campo debe ser valido';
    @Input('input-wrapper') ctrl: NgModel;

    // constructor() {
    //     super();
    // }

    // ngOnInit() {
    //     this._subscription = this.ctrl?.update?.subscribe(
    //         () => this._changes$.next(this.ctrl?.errors),
    //         (error) => console.error(error),
    //         () => this._subscription.unsubscribe()
    //     );
    // }
}

