import { ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ManagerDataService } from '../../service/service.manager';
import { FormAction } from './form.component';
import { NgForm } from '@angular/forms';
import { ActionEvent } from '@shared/model/crud.model';

export class ManagerDataForm<T, C extends ManagerDataService<T>> {

    @ViewChild('form') form: NgForm;

    constructor(public service: C) { }

    onAction(change: ActionEvent<FormAction>) {
        switch (change.action) {
            case FormAction.Submit:
                this.service.onAdd(this.form);
                break;

            case FormAction.Cancel:
                this.service.onCancel(this.form);
                break;

            default:
                throw new Error(`Accion no implementada ${FormAction[change.action]}`);
        }
    }

}
