import { Component, Input, HostListener, Output, EventEmitter, ContentChild, AfterViewInit, ElementRef, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { KEY_EVENT } from '@shared/tools';
import { ActionEvent } from '@shared/model/crud.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';

export enum FormAction {
    Submit,
    Cancel,
    Reset,
    Edit
}

@Component({
    // tslint:disable-next-line
    selector: 'form-template',
    templateUrl: './form.component.html'
})
export class FormTemplateComponent {

    @Input() edit: boolean;
    @Input() titleForm: string;
    @Input() titleIcon = 'mdi mdi-clipboard-text-multiple';
    @Input() addButtonLabel = 'Agregar';
    @Input() saveButtonLabel = 'Guardar';
    @Input() cancelButtonLabel = 'Cancelar';
    @Input() form: NgForm;
    @Input() enableSwitch = true;
    @Input() clearButton = true;
    @Input() actions = true;
    @Output() changes = new EventEmitter();
    @Output() onClose = new EventEmitter();
    @Output() onAdd = new EventEmitter();

    readonly switchId = `switch_${Math.trunc(Math.random() * 100) + 1}`;
    readonly spinnerName = `spinner_${Math.trunc(Math.random() * 100) + 1}`;

    // tslint:disable-next-line
    private _busy: boolean;

    @Input()
    set busy(value: boolean) {
        this._busy = value;
        // prevenir errores como ChangedCheckedException
        this._busy
            ? setTimeout(() => this.spinner.show(this.spinnerName))
            : setTimeout(() => this.spinner.hide(this.spinnerName));
    }

    get busy(): boolean {
        return this._busy;
    }

    // tslint:disable-next-line
    private _addOnlyOne: boolean;

    @Output()
    addOnlyOneChange = new EventEmitter<boolean>();

    @Input()
    get addOnlyOne() {
      return this._addOnlyOne;
    }

    set addOnlyOne(val) {
      this._addOnlyOne = val;
      this.addOnlyOneChange.emit(this._addOnlyOne);
    }

    constructor(public spinner: NgxSpinnerService) { }

    onSubmit() {
        this.changes.emit(
            new ActionEvent<FormAction>(FormAction.Submit)
        );
        this.onAdd.emit();
    }

    onCancel() {
        this.changes.emit(
            new ActionEvent<FormAction>(FormAction.Cancel)
        );
        this.onClose.emit();
    }

}
