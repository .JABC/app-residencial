import { ViewChild, OnDestroy, AfterViewInit, OnInit, Injector, TemplateRef } from '@angular/core';
import { ListTemplateComponent, ListAction } from './list.component';
import { Subscription, EMPTY } from 'rxjs';
import { ManagerDataService } from '../../service/service.manager';
import { IEntityModel } from '@shared/model/entity.model';
import { IManagerMethods, ActionEvent } from '@shared/model/crud.model';
import { appInjector } from '@shared/utils/injector';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { ModalComponent } from '@shared/modal/modal.component';

export class ManagerDataList<T extends IEntityModel, C extends ManagerDataService<T>>
    implements OnInit, AfterViewInit, OnDestroy, IManagerMethods<T> {

    @ViewChild('list') list: ListTemplateComponent;
    @ViewChild('form') form: any;

    protected modalService: ModalService;
    private injector: Injector = appInjector();
    private subscriptions: Subscription[] = [];

    constructor(public service: C/*, public form: any*/) {
        this.modalService = this.injector.get(ModalService);
    }

    ngOnInit() {
        this.service.subscribe();
        this.load();
    }

    ngAfterViewInit() {
        this.subscriptions.push(this.list.changes.subscribe(
            (action: ActionEvent<ListAction>) => this.onAction(action)
        ));
    }

    private onAction(actionEvent: ActionEvent<ListAction>) {
        switch (actionEvent.action) {
            case ListAction.Add:
                this.add();
                break;

            case ListAction.Load:
                this.load();
                break;

            case ListAction.Edit:
                this.update(actionEvent.args);
                break;

            case ListAction.Delete:
                this.delete(actionEvent.args);
                break;

            default:
                break;
        }
    }

    add(model?: T): void {
        this.modalService.showModal(this.form, { type: TypeModal.Data });
    }

    load(filter?: any): void {
        this.service.load();
    }

    update(model?: T): void {
        this.service.onEdit(model);
        this.modalService.showModal(this.form, { type: TypeModal.Data });
    }

    delete(model?: T): void {
        this.modalService.showModal(ModalComponent, { type: TypeModal.Delete })
            .then(() => this.service.delete(model)).catch(() => EMPTY);
    }

    ngOnDestroy() {
        this.service.unsubscribe();
        this.subscriptions.forEach(
            (subscription: Subscription) => subscription.unsubscribe()
        );
        this.subscriptions = [];
    }



}

