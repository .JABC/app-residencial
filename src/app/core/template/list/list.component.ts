import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ActionEvent } from '@shared/model/crud.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManagerDataService } from '@core/service/service.manager';
import { OperationRequest } from '@core/service/service.model';

export enum ListAction {
    Load,
    Add,
    Delete,
    Edit
}

@Component({
    // tslint:disable-next-line
    selector: 'list-template',
    templateUrl: './list.component.html'
})
export class ListTemplateComponent {

    @Input() titleList: string;
    @Input() titleIcon = 'mdi mdi-table-search';
    @Input() service: ManagerDataService<any>;
    // @Input() dataLength: number;
    // @Input() dataRows: number[];
    @Input() canAdd = true;
    @Output() changes = new EventEmitter();
    // @Output() selectedRow = new EventEmitter<number>();

    // tslint:disable-next-line
    private _load: boolean;

    @Input()
    set load(value: boolean) {
        this._load = value;
        // prevenir errores como ChangedCheckedException
        this._load
            ? setTimeout(() => this.spinner.show('LIST_SPINNER'))
            : setTimeout(() => this.spinner.hide('LIST_SPINNER'));
    }

    get load(): boolean {
        return this._load;
    }

    constructor(public spinner: NgxSpinnerService) {}

    onLoad() {
        this.changes.next(
            new ActionEvent<ListAction>(ListAction.Load)
        );
    }

    onAdd() {
        this.changes.next(
            new ActionEvent<ListAction>(ListAction.Add)
        );
    }

    onEdit(model?: any) {
        this.changes.next(
            new ActionEvent<ListAction>(ListAction.Edit, model)
        );
    }

    onDelete(model?: any) {
        this.changes.next(
            new ActionEvent<ListAction>(ListAction.Delete, model)
        );
    }
}
