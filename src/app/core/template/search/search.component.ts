import { Component, ViewChild, TemplateRef, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { ManagerDataService } from '@core/service/service.manager';
import { NgxSpinnerService } from 'ngx-spinner';
import { EMPTY } from 'rxjs';
import { Filter } from '@core/service/service.model';

export class SearchColumn {
    constructor(
        public displayName: string,
        public propertyName: string = displayName) {
    }
}

// tslint:disable
@Component({
    selector: 'search-template',
    templateUrl: './search.component.html'
})
export class SearchTemplateComponent /* implements OnDestroy */ {

    @ViewChild('searchModal') searchModal: TemplateRef<any>;

    @Input() modalSize: 'sm' | 'lg' | 'xl' = 'lg';
    @Input() searchTitle: string;
    @Input() service: ManagerDataService<any>;
    @Input() columns: SearchColumn[];
    @Input() filter: Filter[] = [];
    @Output() onSelect = new EventEmitter<any>();

    selected: any;

    private _busy: boolean;

    @Input()
    set busy(value: any) {
        this._busy = value;
        // prevenir errores como ChangedCheckedException
        this._busy
        ? setTimeout(() => this.spinner.show('SEARCH_SPINNER'))
        : setTimeout(() => this.spinner.hide('SEARCH_SPINNER'));
    }

    get busy() {
        return this._busy;
    }

    constructor(public modal: ModalService, public spinner: NgxSpinnerService) {

    }

    show() {
        this.modal.showModal(this.searchModal, {
            type: TypeModal.Data,
            size: this.modalSize
        });
        this.load();
    }

    load() {
        this.service.itemsPerPage.unshift(10);
        this.service.config.Pagination.Limit = 10;
        this.service.config.Filters = [...this.filter];
        this.service.load();
        this.selected = null;
    }

    select() {
        this.onSelect.emit(this.selected);
        this.selected = null;
        this.close();
    }

    close() {
        this.modal.closeModal();
        // Limpia data para no presentar data anterior cuando vuelva abrirse
        this.service.data = [];
    }

}
