import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit {

    @Input() total: number; // The number of items in your paginated collection.
    // tslint:disable-next-line
    private _page: number;

    @Input()
    get page() {
        return this._page;
    }

    set page(value: number) {
        this._page = value;
    }

    @Output() pageChange = new EventEmitter();

    @Input() limit: number; // The number of items per page.
    @Input() totalPage = 5; // The maximum number of pages to display.
    @Input() autohide = true;
    @Input() size: 'sm' | 'lg';

    constructor() { }

    ngOnInit(): void { }
}
