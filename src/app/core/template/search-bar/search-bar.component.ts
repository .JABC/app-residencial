import { Component, Input, ViewChild, TemplateRef, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ManagerDataService } from '../../service/service.manager';
import { ModalService } from '@shared/modal/modal.service';
import { Subscription, fromEvent } from 'rxjs';
import { map, filter, debounceTime, tap, distinctUntilChanged } from 'rxjs/operators';
import { Filter } from '@core/service/service.model';
import { SearchColumn } from '../search/search.component';

@Component({
    // tslint:disable-next-line
    selector: 'search-bar',
    templateUrl: './search-bar.component.html',
})
export class SearchBarComponent implements OnInit, OnDestroy {

    @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;

    @Input() columns: SearchColumn[];
    @Input() service: ManagerDataService<any>;
    @Input() filter: Filter[] = []; // @Input() filter: any;

    private subscription: Subscription;
    // tslint:disable-next-line
    private _activeColumn: SearchColumn;

    @Input()
    public set activeColumn(value: SearchColumn) {
        this._activeColumn = value;
        this.placeholder = `Buscar por ${this._activeColumn.displayName}`;
    }

    public get activeColumn(): SearchColumn {
        this._activeColumn = this._activeColumn || this.columns[0];
        return this._activeColumn;
    }

    placeholder: string;
    searchValue: string;

    constructor(public element: ElementRef<any>) { }

    ngOnInit() {
        const key = fromEvent(this.element.nativeElement, 'keyup')
            .pipe(
                map((e: any) => e.target.value),
                debounceTime(2500),
                distinctUntilChanged(),
                tap((value: string) => this.searchValue = value)
            );

        this.subscription = key.subscribe(() => this.search());
    }

    search() {
        const newFilter = new Filter(this._activeColumn.propertyName, this.searchValue);
        console.log('Filter:\n', newFilter);
        this.service.config.Filters = [...this.filter, newFilter];
        this.service.config.Pagination.Page = 1;
        this.service.load();
    }

    selectColumn(column: SearchColumn) {
        this.activeColumn = column;
        setTimeout(() => this.searchInput.nativeElement.focus());
    }

    ngOnDestroy() {
        // Elimina los filtros almacenado por este componente
        this.service.config.Filters = [];
        this.subscription.unsubscribe();
        this.subscription = null;
    }

}
