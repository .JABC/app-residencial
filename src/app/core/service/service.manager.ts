import { Subject, Subscription, BehaviorSubject, EMPTY, Observable } from 'rxjs';
import { ErrorService } from '@shared/utils/Errors/ErrorService';
import { appInjector } from '@shared/utils/injector';
import { Injector, InjectionToken } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IManagerMethods } from '@shared/model/crud.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastService, TypeToast } from '@shared/toast/toast.service';
import { IEntityModel } from '@shared/model/entity.model';
import { NgbModal, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ModalComponent } from '@shared/modal/modal.component';
import { OperationRequest, Filter, Pagination, OperationResponse } from './service.model';

export const API_URL_TOKEN = new InjectionToken('API_URL');

export enum ServiceState {
    Safe,
    Add,
    Update,
    Busy,
    Load
}

export class ServiceInitial {

    private injector: Injector = appInjector();

    toast: ToastService;
    modal: ModalService;
    protected http: HttpClient;
    protected errorService: ErrorService;
    protected API_URL: any;

    constructor() {
        this.inicialize();
    }

    private inicialize() {
        this.http = this.injector.get(HttpClient);
        this.API_URL = this.injector.get(API_URL_TOKEN);
        this.toast = this.injector.get(ToastService);
        this.modal = this.injector.get(ModalService);
        this.errorService = this.injector.get(ErrorService);
    }
}

export class ServiceRequest<TModel> extends ServiceInitial {

    protected onError$ = new Subject();

    constructor(public ctrlName: string) {
        super();
    }

    getMethod(callback?: (result?: any) => void, config?: any) {
        // let params = new HttpParams();
        // const headers = new HttpHeaders({ 'Content-type': 'application/json' });
        // Object.keys(config).forEach(x => params = params.append(x, config[x]));
        const request = this.http.get(`${this.API_URL}/${this.ctrlName}/select` );
        const req = request.subscribe(
            (result: any) => {
                if (callback) {
                    callback(result);
                }
                (req as Subscription).unsubscribe();
            },
            (error: any) => this.onError$.next(error)
        );
    }

    postMethod(apiMethod: string, body: TModel | any, callback?: (result?: any) => void) {
        const request = this.http.post(`${this.API_URL}/${this.ctrlName}/${apiMethod}`, body);
        const req = request.subscribe(
            (result: any) => {
                if (callback) {
                    callback(result);
                }
                (req as Subscription).unsubscribe();
            },
            (error: any) => this.onError$.next(error)
        );
    }

    // deleteMethod(apiMethod: string, body: T, callback?: (result: any) => void) {
    //     const request = this.http.post(`${this.API_URL}/${this.ctrlName}/${apiMethod}`, body);
    //     this.onState$.next(ServiceState.Busy);
    //     const req = request.subscribe(
    //         (result: any) => {
    //             callback(result);
    //             this.onState$.next(ServiceState.Safe);
    //             (req as Subscription).unsubscribe();
    //         },
    //         (error: any) => this.onError$.next(error)
    //     );
    // }

    // putMethod(apiMethod: string, body: T, callback?: (result: any) => void) {
    //     const request = this.http.post(`${this.API_URL}/${this.ctrlName}/${apiMethod}`, body);
    //     this.onState$.next(ServiceState.Busy);
    //     const req = request.subscribe(
    //         (result: any) => {
    //             callback(result);
    //             this.onState$.next(ServiceState.Safe);
    //             (req as Subscription).unsubscribe();
    //         },
    //         (error: any) => this.onError$.next(error)
    //     );
    // }

}

export class ManagerDataService<TModel extends IEntityModel> extends ServiceRequest<TModel>
    implements IManagerMethods<TModel> {

    protected onState$ = new Subject<ServiceState>();
    private subscriptions: Subscription[] = [];

    // Propiedades de informacion
    // Al realizar un accion emite un argumento
    onAdded = new Subject<TModel>();
    onEdited = new Subject<TModel>();
    onDeleted = new Subject<TModel>();
    onLoad = new Subject<TModel[]>();

    form: NgForm;
    model: any = {};
    data = new Array<TModel>(); // previene undefined

    addOnlyOne = false;
    editing = false;
    busy = false;
    itemsPerPage = [5, 10, 15, 30];
    config = new OperationRequest();

    // tslint:disable-next-line
    private _loading = new BehaviorSubject<boolean>(false);

    get loading(): Observable<boolean> {
        return this._loading.asObservable();
    }

    constructor(controllerName: string) {
        super(controllerName);
        this.config.Pagination.Limit = this.itemsPerPage[0];
    }

    subscribe() {
        this.subscriptions.push(
            this.onError$.subscribe(
                error => {
                    this.errorService.handleError(error);
                    this.busy = false;
                    this._loading.next(false);
                }
            )
        );
        this.subscriptions.push(
            this.onState$.subscribe(
                (state: ServiceState) => this.onState(state)
            )
        );
    }

    unsubscribe() {
        this.subscriptions.forEach(
            (subscription: Subscription) => subscription.unsubscribe()
        );
        this.subscriptions = [];
    }

    private onState(state: ServiceState) {
        // this.toast.show({ body: `${ServiceState[state]}`, type: TypeToast.Notification });
        /* ^^^^^^^^^^^^^^ Para ver ejecucion de estados ^^^^^^^^^ */
        switch (state) {
            case ServiceState.Busy:
                this.busy = true;
                break;

            case ServiceState.Load:
                this.data = [];
                this._loading.next(true);
                break;

            case ServiceState.Add:
                this.onReset(this.form);
                break;

            case ServiceState.Update:
                this.editing = true;
                break;

            default:
                this.busy = false;
                this.editing = false;
                this.addOnlyOne = false;
                this._loading.next(false);
                break;
        }
    }

    /* ==== Metodos para formulario ==== */

    // inicializa un form
    // luego llama al metodo add
    onAdd(form: NgForm) {
        this.form = form;
        console.log(this.model);
        this.add();
    }

    // Verifica si el modal fue aceptado o denegado para borrar el item
    // onDelete(model?: T) {
    //     this.modal.showModal(ModalComponent, { type: TypeModal.Delete })
    //         .then(() => this.delete(model))
    //         .catch(() => EMPTY);
    // }

    // Cambia el estado de la app a update el cual coloca el formulario
    // en modo de editar y asigna el model recibido al existente
    onEdit(model?: TModel) {
        Object.assign(this.model, model);
        this.onState$.next(ServiceState.Update);
    }

    // Limpia formulario
    onReset(form: NgForm) {
        try {
            for (const control in form.controls) {
                if (form.controls.hasOwnProperty(control)) {
                    form.controls[control].reset();
                    form.controls[control].setErrors(null);
                    form.controls[control].markAsUntouched();
                }
            }
            form.reset();
        } catch {
            return;
        }
        this.model = {};
    }

    // Limpia modelo para eliminar bug de consistencia del modelo anterior
    // Cierra la ventana modal
    onCancel(form?: any) {
        this.onState$.next(ServiceState.Safe);
        this.onReset(form);
        try {
            this.modal.dismissModal();
        } catch {
            return;
        }
    }

    // Paginacion
    // onPaginate(target: any) {
    //     if (target.hasOwnProperty('limit')) {
    //         this.config.Pagination.Limit = target?.limit;
    //         this.config.Pagination.Page = 1;
    //     } else if (target.hasOwnProperty('page')) {
    //         this.config.Pagination.Page = target?.page;
    //     }

    //     this.load();
    // }

    /* =================================================================== */

    add(model?: TModel) {
        const item = model || this.model;
        if (this.editing) {
            this.update();
        } else {
            this.onState$.next(ServiceState.Busy);
            this.postMethod('add', item, (result: any) => {
                if (!result) {
                    return;
                }

                this.onAdded.next(result);

                this.addOnlyOne
                    ? this.onCancel(this.form)
                    : this.onState$.next(ServiceState.Add);

                this.load();
            });
        }
    }

    load(config?: OperationRequest) {
        this.onState$.next(ServiceState.Load);
        Object.assign(this.config, config);
        console.log('Config:\n', this.config);
        this.postMethod('select', this.config, (result: OperationResponse<TModel>) => {
            this.data = result.Data;
            this.onLoad.next(result.Data);
            this.config.Pagination.TotalCount = result.TotalCount;
            console.log('Data:\n', result.Data, '\nTotal:\n', result.TotalCount);
            this.onState$.next(ServiceState.Safe);
        });
    }

    update(model?: TModel) {
        this.onState$.next(ServiceState.Busy);
        const item = model || this.model;
        this.postMethod(`update`, item, (result: any) => {
            if (!result) {
                return;
            }
            this.onEdited.next(result);
            this.onCancel(this.form);
            this.load();
        });
    }

    delete(model?: TModel) {
        this.onState$.next(ServiceState.Load);

        const item = model || this.model;
        this.postMethod(`delete`, item, (result: any) => {
            if (!result) {
                return;
            }

            this.onDeleted.next(result);
            this.load();
        });
    }


}

