import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ManagerDataList } from '@core/template/list/list.manager';
import { INoticiaModel } from '../noticia.model';
import { NoticiaService } from '../noticia.service';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
})
export class NoticiaListComponent extends ManagerDataList<INoticiaModel, NoticiaService>
  implements OnInit, OnDestroy, AfterViewInit {

    columns: SearchColumn[] = [
        new SearchColumn('Titulo')
    ];

    constructor(public service: NoticiaService) {
        super(service);
    }

    ngOnInit(): void { }
}
