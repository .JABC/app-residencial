import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { NoticiaFormComponent } from './form.component';
import { NoticiaService } from '../noticia.service';

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [NoticiaFormComponent],
    exports: [NoticiaFormComponent],
    providers: [NoticiaService]
})
export class NoticiaFormModule { }
