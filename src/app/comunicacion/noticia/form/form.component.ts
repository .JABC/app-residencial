import { Component, OnInit } from '@angular/core';
import { ManagerDataForm } from '@core/template/form/form.manager';
import { INoticiaModel } from '../noticia.model';
import { NoticiaService } from '../noticia.service';

@Component({
    selector: 'app-area-comun-form',
    templateUrl: './form.component.html',
})
export class NoticiaFormComponent extends ManagerDataForm<INoticiaModel, NoticiaService>
    implements OnInit {

    constructor(public service: NoticiaService) {
        super(service);
    }

    ngOnInit(): void { }
}
