import { Component, OnInit } from '@angular/core';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-noticia',
    templateUrl: './noticia.component.html'
})
export class NoticiaComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }

}
