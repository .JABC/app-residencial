import { Injectable } from '@angular/core';
import { ManagerDataService } from '@core/service/service.manager';
import { INoticiaModel } from './noticia.model';

@Injectable({ providedIn: 'root' })
export class NoticiaService extends ManagerDataService<INoticiaModel> {

    constructor() {
        super('noticia');
    }

}
