import { Routes, RouterModule } from '@angular/router';
import { NoticiaComponent } from './noticia.component';
import { NoticiaListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { NoticiaFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: NoticiaComponent,
        children: [{ path: '', component: NoticiaListComponent }]
    }
];

@NgModule({
    declarations: [
        NoticiaComponent,
        NoticiaListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        NoticiaFormModule,
        RouterModule.forChild(routes)
    ]
})
export class NoticiaModule { }
