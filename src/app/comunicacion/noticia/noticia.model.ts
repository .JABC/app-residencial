import { IEntityModel } from '@shared/model/entity.model';

export interface INoticiaModel extends IEntityModel {
    Titulo: string;
    Cuerpo: string;
}
