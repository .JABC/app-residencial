import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'noticia', pathMatch: 'full' },
  {
    path: '',
    children: [
      {
        path: 'noticia',
        loadChildren: () => import('./noticia/noticia.module')
                            .then(m => m.NoticiaModule),
        data: { title: 'Adminitrar Noticias' }
      },
      {
        path: 'comunicado',
        loadChildren: () => import('./comunicado/comunicado.module')
                            .then(m => m.ComunicadoModule),
        data: { title: 'Adminitrar Comunicados' }
      }
    ]
  }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)
    ]
})
export class ComunicacionModule { }
