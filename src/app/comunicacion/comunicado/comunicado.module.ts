import { Routes, RouterModule } from '@angular/router';
import { ComunicadoComponent } from './comunicado.component';
import { ComunicadoListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ComunicadoFormModule } from './form/form.module';

const routes: Routes = [
    {
        path: '',
        component: ComunicadoComponent,
        children: [
            {
                path: '',
                component: ComunicadoListComponent
            }
        ]
    }
];

@NgModule({
    declarations: [
        ComunicadoComponent,
        ComunicadoListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        ComunicadoFormModule,
        RouterModule.forChild(routes)
    ]
})
export class ComunicadoModule { }
