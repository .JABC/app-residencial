import { IEntityModel } from '@shared/model/entity.model';

export interface IComunicadoModel extends IEntityModel {
    Titulo: string;
    Cuerpo?: string;
    Clasificacion: string;
}

export enum EClasificacion {
    Solicitud = 'Solicitud',
    Sugerencia = 'Sugerencia',
    Queja = 'Queja'
}
