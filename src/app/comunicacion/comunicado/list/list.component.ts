import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ManagerDataList } from '@core/template/list/list.manager';
import { IComunicadoModel, EClasificacion } from '../comunicado.model';
import { ComunicadoService } from '../comunicado.service';
import { SearchColumn } from '@core/template/search/search.component';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html'
})
export class ComunicadoListComponent extends ManagerDataList<IComunicadoModel, ComunicadoService>
  implements OnInit, OnDestroy, AfterViewInit {

  columns: SearchColumn[] = [
    new SearchColumn('Titulo')
  ];

  constructor(public service: ComunicadoService) {
    super(service);
  }

  ngOnInit() {
    super.ngOnInit();
    this.service.data = [
      {
        Titulo: 'Titulo 1',
        Clasificacion: EClasificacion.Solicitud
      },
      {
        Titulo: 'Titulo 2',
        Clasificacion: EClasificacion.Queja
      },
      {
        Titulo: 'Titulo 3',
        Clasificacion: EClasificacion.Queja
      },
      {
        Titulo: 'Titulo 4',
        Clasificacion: EClasificacion.Solicitud
      },
      {
        Titulo: 'Titulo 5',
        Clasificacion: EClasificacion.Sugerencia
      },
      {
        Titulo: 'Titulo 6',
        Clasificacion: EClasificacion.Queja
      },
      {
        Titulo: 'Titulo 7',
        Clasificacion: EClasificacion.Sugerencia
      },
      {
        Titulo: 'Titulo 8',
        Clasificacion: EClasificacion.Solicitud
      }
    ];
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

}
