import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ComunicadoFormComponent } from './form.component';
import { ComunicadoService } from '../comunicado.service';

@NgModule({
    imports: [CommonModule, SharedModule],
    declarations: [ComunicadoFormComponent],
    exports: [ComunicadoFormComponent],
    providers: [ComunicadoService]
})
export class ComunicadoFormModule { }
