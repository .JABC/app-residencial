import { Component, OnInit, OnDestroy } from '@angular/core';
import { ManagerDataForm } from '@core/template/form/form.manager';
import { IComunicadoModel } from '../comunicado.model';
import { ComunicadoService } from '../comunicado.service';

@Component({
    selector: 'app-comunicado-form',
    templateUrl: './form.component.html'
})
export class ComunicadoFormComponent extends ManagerDataForm<IComunicadoModel, ComunicadoService>
  implements OnInit, OnDestroy {

  constructor(public service: ComunicadoService) {
    super(service);
  }

  ngOnInit() {
    // super.ngOnInit();
  }

  ngOnDestroy() {
    // super.ngOnDestroy();
  }

}
