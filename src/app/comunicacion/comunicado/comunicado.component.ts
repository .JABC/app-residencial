import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppTitle } from '@shared/tools';

@Component({
    selector: 'app-comunicado',
    templateUrl: './comunicado.component.html'
})
export class ComunicadoComponent extends AppTitle {

    constructor(public route: ActivatedRoute) {
        super(route);
    }
}
