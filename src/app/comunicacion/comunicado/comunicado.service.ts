import { Injectable } from '@angular/core';
import { ManagerDataService } from '@core/service/service.manager';
import { IComunicadoModel, EClasificacion } from './comunicado.model';

@Injectable({
    providedIn: 'root'
})
export class ComunicadoService extends ManagerDataService<IComunicadoModel> {

    clasificacion = [];

    constructor() {
        super('comunicado');
        this.setClasificacion();
    }

    private setClasificacion() {
        for (const item in EClasificacion) {
          if (isNaN(Number(item))) {
            this.clasificacion.push({
              label: item,
              value: EClasificacion[item]
            });
          }
        }
      }

}
