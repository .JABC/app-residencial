import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { MiResidenciaService } from '../mi-residencia.service';
import { SearchColumn } from '@core/template/search/search.component';
import { TypeModal } from '@shared/modal/modal.model';
import { Filter, EFilterOperator } from '@core/service/service.model';

@Component({
    selector: 'app-visitas-entrada',
    templateUrl: './visitas-entrada.component.html'
})
export class VisitaEntradaComponent implements OnInit {

    @ViewChild('component') component: TemplateRef<any>;

    constructor(public modal: ModalService, public service: MiResidenciaService) { }

    ngOnInit(): void {

    }

    show() {
        this.modal.showModal(this.component,
            {
                type: TypeModal.Data,
                size: 'lg'
            });
    }

    close() {
        this.modal.closeModal();
    }

    save() {
        this.service.darEntrada();
        this.close();
    }

}
