import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { MiResidenciaService } from './mi-residencia.service';
import { AppTitle } from '@shared/tools';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-mi-residencia',
    templateUrl: './mi-residencia.component.html'
})
export class MiResidenciaComponent extends AppTitle implements OnInit, OnDestroy {

    @ViewChild('visita') visitaView: any;
    @ViewChild('code') residenciaCode: any;

    constructor(
        public modal: ModalService,
        public service: MiResidenciaService,
        public route: ActivatedRoute
    ) { super(route); }

    ngOnInit(): void {
        this.service.subscribe();
    }

    showVisita() {
        this.modal.showModal(this.visitaView, { type: TypeModal.Data });
    }

    ngOnDestroy() {
        this.service.unsubscribe();
    }
}
