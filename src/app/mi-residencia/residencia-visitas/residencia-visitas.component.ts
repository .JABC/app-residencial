import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { MiResidenciaService } from '../mi-residencia.service';
import { SearchColumn } from '@core/template/search/search.component';
import { ModalComponent } from '@shared/modal/modal.component';
import { EMPTY } from 'rxjs';
import { VisitaEntradaComponent } from '../visitas-entrada/visitas-entrada.component';
import { IVisitaModel } from '@adm/visita/visita.model';
import { Filter, EFilterOperator } from '@core/service/service.model';
import { DateTime } from '@shared/tools';
import { NgModel } from '@angular/forms';

@Component({
    selector: 'app-residencia-visitas',
    templateUrl: './residencia-visitas.component.html'
})
export class ResidenciaVisitasComponent implements OnInit {

    @ViewChild('entrada') visitaEntrada: VisitaEntradaComponent;
    @ViewChild('today') todayControl: NgModel;

    columns: SearchColumn[] = [
        new SearchColumn('Nombre', 'VisitanteNombre'),
        new SearchColumn('Llegada')
    ];

    constructor(public modal: ModalService, public service: MiResidenciaService) { }

    ngOnInit(): void {
        this.service.load();
    }

    reportar(model: IVisitaModel) {
        this.modal.showModal(ModalComponent, {
            title: model.Reportado ? 'Deshacer Reportado' : 'Colocar a Reportado',
            message: '¿Estas seguro de querer realizar esta acción?',
            type: TypeModal.Question
        }).then(() => {
            model.Reportado = !model.Reportado;
            this.service.model = model;
            this.service.reportar();
        }).catch(() => EMPTY);
    }

    darEntrada(model: IVisitaModel) {
        this.service.model = model;
        if (model.DarEntrada) {
            this.modal.showModal(ModalComponent, {
                title: 'Deshacer la visita Programada',
                message: '¿Estas seguro de querer realizar esta acción?',
                type: TypeModal.Question
            }).then(() => {
                this.service.model.DarEntrada = null;
                this.service.darEntrada();
            }).catch(() => EMPTY);
            return;
        }

        this.visitaEntrada.show();
    }

    close() {
        this.modal.closeModal();
    }
}
