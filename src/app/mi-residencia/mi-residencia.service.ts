import { Injectable } from '@angular/core';
import { ManagerDataService, ServiceState } from '@core/service/service.manager';
import { SortFilter, ESortOperator, Filter, EFilterOperator, OperationRequest } from '@core/service/service.model';
import { IVisitaModel } from '@adm/visita/visita.model';
import { UniqueArray } from '@shared/tools';

@Injectable()
export class MiResidenciaService extends ManagerDataService<IVisitaModel> {

    constructor() {
        super('visita');
        this.config.Sorts.push(new SortFilter('Llegada', ESortOperator.Descendent));
        this.model.today = true;
    }

    load() {
        const residenciaId = new Filter('ResidenciaId', this.residencia?.Id, EFilterOperator.Equals);
        const visitasHoy = new Filter('Llegada', new Date().toISOString().split('T')[0]);

        UniqueArray.add(this.config.Filters, residenciaId);

        if (this.model.today) {
            UniqueArray.add(this.config.Filters, visitasHoy);
        } else {
            UniqueArray.delete(this.config.Filters, visitasHoy);
        }

        super.load();
    }

    reportar() {
        this.onState$.next(ServiceState.Load);
        this.postMethod('reportar', this.model, (result: boolean) => {
            if (!result) {
                return;
            }
            this.load();
        });
    }

    darEntrada() {
        this.onState$.next(ServiceState.Load);
        this.postMethod('darEntrada', this.model, (result: boolean) => {
            if (!result) {
                return;
            }
            this.load();
        });
    }

    get residencia(): any {
        const residencia = localStorage.getItem('residencia');
        if (residencia === null) {
            return null;
        }

        const decodeResidencia = atob(residencia);
        return JSON.parse(decodeResidencia);
    }

}
