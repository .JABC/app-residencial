import { Component, OnInit, TemplateRef, ViewChild, OnDestroy } from '@angular/core';
import { ModalService } from '@shared/modal/modal.service';
import { TypeModal } from '@shared/modal/modal.model';
import { Filter, EFilterOperator } from '@core/service/service.model';
import { Subscription } from 'rxjs';
import { MiResidenciaService } from '../mi-residencia.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ResidenciaService } from '@adm/residencia/residencia.service';

@Component({
    selector: 'app-residencia-code',
    templateUrl: './residencia-code.component.html',
    providers: [ResidenciaService]
})
export class ResidenciaCodeComponent implements OnInit, OnDestroy {

    @ViewChild('component') componentViewChild: any;
    residenciaCode = '';
    private subscription: Subscription;

    constructor(public service: ResidenciaService) { }

    ngOnInit(): void {
        this.service.subscribe();
        this.subscription = this.service.onLoad.subscribe(
            (data: any[]) => this.saveResidencia(data)
        );
    }

    ready() {
        const filter = new Filter('UniqueIdentifier', this.residenciaCode, EFilterOperator.Equals);
        this.service.config.Filters = [filter];
        this.service.load();
    }

    open() {
        this.service.modal.showModal(this.componentViewChild, { type: TypeModal.Data, size: 'lg' });
    }

    private saveResidencia(data: any[]) {
        if (data.length === 0) {
            throw new HttpErrorResponse({ error: 'El codigo es inválido' });
        }

        try {
            const residencia = {
                Id: data[0]?.Id,
                Numero: data[0]?.Numero,
                Propietario: data[0]?.PropietarioNombreCompleto
            };
            const encodeResidencia = btoa(JSON.stringify(residencia));
            localStorage.setItem('residencia', encodeResidencia);
            this.service.modal.closeModal();
        } catch (e) {
            throw e;
        }
    }

    close() {
        this.service.modal.closeModal();
    }

    ngOnDestroy() {
        this.service.unsubscribe();
        this.subscription.unsubscribe();
    }

}
