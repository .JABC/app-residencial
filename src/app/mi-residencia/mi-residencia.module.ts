import { SharedModule } from '@shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MiResidenciaComponent } from './mi-residencia.component';
import { ResidenciaVisitasComponent } from './residencia-visitas/residencia-visitas.component';
import { MiResidenciaService } from './mi-residencia.service';
import { VisitaEntradaComponent } from './visitas-entrada/visitas-entrada.component';
import { ResidenciaCodeComponent } from './residencia-code/residencia-code.component';

const routes: Routes = [
    {
        path: '',
        component: MiResidenciaComponent,
        data: { title: 'Residencia' }
    }
];

@NgModule({
    declarations: [
        MiResidenciaComponent,
        ResidenciaVisitasComponent,
        VisitaEntradaComponent,
        ResidenciaCodeComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [MiResidenciaService],
})
export class MiResidenciaModule { }
